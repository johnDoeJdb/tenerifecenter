module.exports = function(grunt) {
    grunt.initConfig({
        copy: {
            fonts: {
                files: [
                    {
                        expand: true,
                        src: './bower_components/bootstrap/fonts/*',
                        dest: './public/assets/fonts/',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: './app/assets/javascript/redactor/redactor-font.eot',
                        dest: './public/assets/fonts/',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            },
            includes: {
                files: [
                    {
                        expand: true,
                        src: './app/assets/includes/*',
                        dest: './public/includes/',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: './app/assets/includes/select/*',
                        dest: './public/includes/select',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: './app/assets/storage/*',
                        dest: './public/storage/',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            },
            paginator: {
                files: [
                    {
                        expand: true,
                        src: './bower_components/angular-utils-pagination/dirPagination.tpl.html',
                        dest: './public/assets/templates/parts/',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            }
        },
        concat: {
            js_site_old: {
                options: {
                    separator: ';'
                },
                src: [
                    './bower_components/jquery/dist/jquery.js',
                    './bower_components/lodash/lodash.js',
                    './bower_components/angular/angular.js',
                    './bower_components/angular-route/angular-route.js',
                    './bower_components/angular-cookies/angular-cookies.js',
                    './bower_components/angular-sanitize/angular-sanitize.js',
                    './bower_components/angular-bootstrap/ui-bootstrap.js',
                    './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                    //'./bower_components/gmaps-markerclusterer-plus/src/markerclusterer.js',
                    //'./bower_components/angular-virtual-scroll/angular-virtual-scroll.js',
                    './app/assets/javascript/site.js'
                ],
                dest: './public/assets/javascript/site_old.js'
            },
            js_site_new: {
                options: {
                    separator: ';',
                },
                src: [
                    './bower_components/jquery/dist/jquery.min.js',
                    './bower_components/jcarousel/dist/jquery.jcarousel.min.js',
                    './app/assets/includes/jcarousel.responsive.js',
                    './bower_components/tether/dist/js/tether.min.js',
                    './bower_components/tether-select/dist/js/select.min.js',
                    './bower_components/lightbox2/dist/js/lightbox-plus-jquery.min.js',
                    './app/assets/includes/index-map.js',
                    './app/assets/includes/rent-map.js',
                    './app/assets/includes/sale-map.js',
                    './app/assets/includes/scripts.js',
                ],
                dest: './public/assets/javascript/site.js'
            },
            js_site_libs: {
                options: {
                    separator: '',
                    nonull: true
                },
                src: [
                    './bower_components/angular/angular.min.js',
                    './bower_components/jquery/dist/jquery.min.js',
                    './bower_components/lightbox2/dist/js/lightbox-plus-jquery.min.js',
                    './bower_components/jquery-ui/ui/datepicker.js',
                    //'./bower_components/jcarousel/dist/jquery.jcarousel.js',
                    //'./app/assets/includes/jcarousel.responsive.js',
                    //'./bower_components/tether/dist/js/tether.min.js',
                    //'./bower_components/tether-select/dist/js/select.min.js',
                ],
                dest: './public/assets/javascript/site-libs.js'
            },
            css_old: {
                options: {
                    separator: ' '
                },
                src: [
                    './app/assets/stylesheets/site.css',
                ],
                dest: './public/assets/stylesheets/site_old.css'
            },
            css_site_libs: {
                options: {
                    separator: ' '
                },
                src: [
                    './app/assets/includes/normalize.css',
                    './app/assets/includes/select/select-theme-default.css',
                    './app/assets/includes/select/select-theme-action.css',
                    './app/assets/includes/select/select-theme-time.css',
                ],
                dest: './public/assets/stylesheets/site_libs.css'
            },
            css_site_main: {
                options: {
                    separator: ' '
                },
                src: [
                    './app/assets/includes/style.css',
                ],
                dest: './public/assets/stylesheets/site_main.css'
            },
            js_admin: {
                options: {
                    separator: ';',
                },
                src: [
                    //'./bower_components/angular-file-upload/dist/angular-file-upload.min.js',
                    './bower_components/lodash/lodash.js',
                    './bower_components/jquery/dist/jquery.js',
                    './bower_components/angular/angular.js',
                    './bower_components/angular-route/angular-route.js',
                    './bower_components/angular-bootstrap/ui-bootstrap.js',
                    './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                    './bower_components/angular-utils-pagination/dirPagination.js',
                    './bower_components/angular-mask/dist/ngMask.js',
                    './bower_components/moment/moment.js',
                    './bower_components/moment/locale/ru.js',
                    './bower_components/moment/locale/en.js',
                    './bower_components/moment/locale/es.js',
                    './bower_components/moment/locale/de.js',
                    './app/assets/javascript/redactor/redactor.min.js',
                    './app/assets/javascript/redactor/table.js',
                    './app/assets/javascript/redactor/imagemanager.js',
                    './app/assets/javascript/redactor/ru.js',
                    './app/assets/javascript/redactor/angular-redactor.js',
                    './app/assets/javascript/admin.js'
                ],
                dest: './public/assets/javascript/admin.js'
            }
        },
        less: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "./app/assets/stylesheets/site.css": [
                        './app/assets/stylesheets/site.less'
                    ],
                    "./public/assets/stylesheets/admin.css": [
                        './app/assets/stylesheets/admin.less',
                        './app/assets/javascript/redactor/redactor.less'
                    ]
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: './public/assets/stylesheets/',
                    src: ['site.css'],
                    dest: './public/assets/stylesheets/',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            my_target: {
                files: {
                    'public/assets/javascript/site.min.js': ['public/assets/javascript/site.js']
                }
            }
        }
        //uglify: {
        //    options: {
        //        mangle: false,
        //        preserveComments: false
        //    },
        //    site: {
        //        files: {
        //            './public/assets/javascript/site.min.js': ['./public/assets/javascript/site.js']
        //        }
        //    },
        //    admin: {
        //        files: {
        //            './public/assets/javascript/admin.js': ['./public/assets/javascript/admin.js']
        //        }
        //    }
        //}
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-stripcomments');
    grunt.loadNpmTasks('grunt-phpunit');

    grunt.registerTask('compile', ['less', 'copy', 'concat']); // , 'uglify'
    grunt.registerTask('js-site', ['comments', 'concat:js_site_libs', 'concat:js_site_new']);
    grunt.registerTask('css-site', ['copy:includes']);
    grunt.registerTask('uglify', ['uglify']);
    grunt.registerTask('minify', ['cssmin']); // , 'uglify'
    grunt.registerTask('t', ['concat:js_admin']);

};