/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

                var map;
                function initialize() {
                    map = new google.maps.Map(document.querySelector('.b-map__index'), {
                      zoom: 10,
                      center: {lat: 28.2170518, lng:-16.6095194},
                      scrollwheel: false,
                      disableDefaultUI: true,
                      zoomControl: true,
                      zoomControlOptions: {
                                style: google.maps.ZoomControlStyle.SMALL,
                                position: google.maps.ControlPosition.RIGHT_TOP
                       }
                   } );
                
                    var marker = new google.maps.Marker({
                        position: {lat: 28.0745848, lng:-16.6954544},
                        map: map,
                        title:"Chayofa Country Club" ,
                        url:"//www.google.com/maps/preview/@28.0745848,-16.6954544,14z"
                    });
                    
                    google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });
                    
                    var marker = new google.maps.Marker({
                        position: {lat: 28.4749785, lng:-16.4341492},
                        map: map,
                        title:"El Sauzal" ,
                        url:"//www.google.com/maps/preview/@28.4749785,-16.4341492,14z"
                    });      
                    
                    google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });
                    
                    var marker = new google.maps.Marker({
                        position: {lat: 28.3671432, lng:-16.7137571},
                        map: map,
                        title:"Icod de los Vinos" ,
                        url:"//www.google.com/maps/preview/@28.3671432,-16.7137571,14z",
                    });      
                    
                  google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });
                    
                    var marker = new google.maps.Marker({
                        position: {lat: 28.3053534, lng:-16.8399144},
                        map: map,
                        title:"Masca" ,
                        url:"//www.google.com/maps/preview/@28.3053534,-16.8399144,14z"
                    });  
                    
                    google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });
                    
                    var marker = new google.maps.Marker({
                        position: {lat: 28.2412559, lng:-16.8403286},
                        map: map,
                        title:"Acantilados de los Gigantes" ,
                        url:"//www.google.com/maps/preview/@28.2412559,-16.8403286,14z"
                    });  
                    
                    google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });
                    
                    var marker = new google.maps.Marker({
                        position: {lat: 28.166271, lng:-16.5013701},
                        map: map,
                        title:"Villa de Arico" ,
                        url:"//www.google.com/maps/preview/@28.166271,-16.5013701,14z"
                    });  
                    
                    google.maps.event.addListener(marker, 'click', function() {
                         window.open(this.url,'_blank');
                  });

                }
                
                function detectBrowser() {
                    var useragent = navigator.userAgent;
                    var mapdiv = document.getElementById("map-canvas");

                    if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 ) {
                      mapdiv.style.width = '100%';
                      mapdiv.style.height = '100%';
                    } else {
                      mapdiv.style.width = '600px';
                      mapdiv.style.height = '800px';
                    }
                }

                google.maps.event.addDomListener(window, 'load', initialize);

