/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function swapImage () {
    var image = document.querySelector(".e-language__image");
    var dropdown = document.querySelector(".b-lang");
    image.src = '/includes/b-lang-'+dropdown.value+'.png';
    window.location.pathname = '/'+ dropdown.value;
};

function show(state){
        document.querySelector('.b-login__panel').style.display = state;			
        document.querySelector('.wrap').style.display = state; 			
};

function period(state){
        document.querySelector('.b-time__search').style.display = state;			
        document.querySelector('.e-period-from__search').style.display = state; 
        document.querySelector('.e-period-to__search').style.display = state; 
        if(state === 'none'){$( ".e-keyword__search").width( "72.1%" ); }
       else {$( ".e-keyword__search").width( "28%" ); }
};

function changeImage(source) {
       document.querySelector(".e-form-photo").src = source;
 };
 
 $(function() {  
     jQuery("input[name=calender]").datepicker() ;
 });
 
 if(window.location.pathname === $('.e-main-sale').attr('href')){$('.e-main-sale').addClass('js-e-main-menu-current');};
 if(window.location.pathname === $('.e-main-rent').attr('href')){$('.e-main-rent').addClass('js-e-main-menu-current');};
 if(window.location.pathname === $('.e-main-specoff').attr('href')){$('.e-main-specoff').addClass('js-e-main-menu-current');};
 if(window.location.pathname === $('.e-main-tourism').attr('href')){$('.e-main-tourism').addClass('js-e-main-menu-current');};
 if(window.location.pathname === "/about-team.html"){$('.e-main-sale').addClass('js-e-main-menu-current');};
 
             Select.init({
                selector: '.js-e-choose-value',
                className: 'select-theme-default'
            });
            Select.init({
                selector: '.e-search-action',
                className: 'select-theme-action'
            });
            Select.init({
                selector: '.e-time__search',
                className: 'select-theme-time'
            });