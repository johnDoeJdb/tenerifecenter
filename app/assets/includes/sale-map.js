/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

                var map;
                function initialize() {
                    map = new google.maps.Map(document.querySelector('.b-map__sale'), {
                      zoom: 13,
                      center: {lat: 28.0814668, lng:-16.7031404},
                      scrollwheel: false,
                      disableDefaultUI: true,
                      zoomControl: true,
                      zoomControlOptions: {
                                style: google.maps.ZoomControlStyle.SMALL,
                                position: google.maps.ControlPosition.RIGHT_TOP
                       }
                    });
                
                    var marker = new google.maps.Marker({
                        position: {lat: 28.0745848, lng:-16.6954544},
                        map: map,
                        title:"Chayofa Country Club" ,
                        url:"//www.google.com/maps/preview/@28.0745848,-16.6954544,14z"
                    });       
                    
                    google.maps.event.addListener(marker, 'click', function() {
                    window.open(this.url,'_blank');
                     });
                }
                
                function detectBrowser() {
                    var useragent = navigator.userAgent;
                    var mapdiv = document.getElementById("map-canvas");

                    if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1 ) {
                      mapdiv.style.width = '100%';
                      mapdiv.style.height = '100%';
                    } else {
                      mapdiv.style.width = '600px';
                      mapdiv.style.height = '800px';
                    }
                }

                google.maps.event.addDomListener(window, 'load', initialize);