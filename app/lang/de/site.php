<?php

return [
    'langs' => [
        'ru' => 'Русский',
        'en' => 'English',
        'es' => 'Español',
        'de' => 'Deutsch'
    ]
];