<?php

return [
    'common' => [
        'more' => 'More',
    ],
    'langs' => [
        'ru' => 'Русский',
        'en' => 'English',
        'es' => 'Español',
        'de' => 'Deutsch'
    ],
    'menu' => [
        'about' => 'About us',
        'tourism' => 'tourism',
        'investments' => 'investments',
        'special_offers' => 'special offers',
        'sale' => 'sale',
        'rent' => 'rent',
    ],
    'search' => [
        'type' => 'Property type',
        'place' => 'Location',
        'price_from' => 'Price from',
        'price_to' => 'to',
        'bedrooms' => 'Bedrooms',
        'from' => 'from',
        'code' => 'Code',
        'keywords' => 'Keywords',
        'search' => 'Search',
        'sale' => 'Sale',
        'rent' => 'Rent',
        'region' => 'Region',
        'property_type' => 'Property type',
        'complex' => 'Complex',
        'per_month' => 'Per month',
        'per_day' => 'Per day',
        'free_from' => 'Free from',
        'to' => 'to',
        'keyword_code' => 'Keyword or code',
    ],
    'welcome' => [
        'we_recommend' => 'We recommend',
        'welcome_title' => 'Welcome to VYM Canaries website!',
        'text_line1' =>
            'At Vym Canarias we are not just a real estate company, we are a team of professionals who care about their customers and try
            to serve them the best way we can. We provide our clients with a high level of professionalism and a wide range of services.
            We are highly trained in travel and tourism and can guide you on how to spend your time here in Tenerife taking into account your interests,
            time and budget. Whether you are buying a property with us or just simply just need to rent a car, we are always happy to help.',
        'text_line2' =>
            'When buying a property with us we will be with you from the beginning, during and after the purchase of your home.
            “Vym Canarias” can help you with important issues such as the maintenance and supervision of your home,
            for you to know that even when you are not here to look after your property we will be.',
        'text_line3' =>
            'We would be delighted to suggest to our clients a wide range of our services details of which can be found on our page under the heading “our services”,
             which include all aspects of the property business, from buying and selling property,
             apartment rental to general maintenance of your property or even help organize making repairs.',
        'text_line4' =>
            'Trust the professionals! This will help you save time and money!',
        'text_line5' =>
            'We look forward to seeing you in our offices, conveniently situated in one of the most beautiful and luxurious areas of Southern Tenerife,
             C.C. Victoria Tenerife Sur, Playa de las Americas, find further details on how to get here on our page headed вЂњcontact usвЂќ',
        'text_line6' =>
            'Our offices are open from 10:00 to 19:00 Monday through Friday. Saturday appointments upon request.',
        'text_line7' =>
            "Please send your inquiries to our general email address:
            <br \>info@tenerifecenter.com: - buying and selling property;
            <br \>vym.tenerife@gmail.com - property rent and all touristic services.",
    ],
    'tag' => [
        'special_offers' => 'Special offers',
        'popular_locations' => 'Popular locations',
        'property_types' => 'Property types',
    ],
    'offers' => [
        'offers' => 'Offers',
        'sq_meters' => 'SQ. METERS',
        'rooms' => 'ROOMS',
        'price' => 'PRICE',
        'latest' => 'Latest',
        'lowest_price' => 'Lowest Price',
    ],
    'add_place' => [
        'text' => 'Add for sale or renting out your place',
        'button' => 'Add place',
    ],
    'news' => [
        'latest_news' => 'Latest News',
    ],
    'contacts' => [
        'mobile' => 'Mobile',
        'office' => 'Office',
        'fax' => 'Fax',
    ],
];