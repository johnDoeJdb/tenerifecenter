<?php
return [
    'login' => [
        'email' => 'Email',
        'password' => 'Пароль',
        'memorize' => 'Запомнить',
        'login' => 'Вход',
    ],
    'site' => [
        'title' => 'Административный раздел',
        'logout' => 'Выход',
        'to_site' => 'На сайт'
    ],
    'menu' => [
        'objects' => 'Объекты',
        'news' => 'Новости',
        'pages' => 'Страницы',
        'tags' => 'Теги',
        'places' => 'Места',
        'complexes' => 'Комплексы',
        'banners' => 'Баннеры',
        'offices' => 'Офисы',
        'managers' => 'Менеджеры',
    ],
    'lang' => [
        'translate' => 'Перевод',
        'ru' => 'Русский',
        'en' => 'Английский',
        'es' => 'Испанский',
        'de' => 'Немецкий',
        'from_ru' => 'с русского',
        'from_en' => 'с английского',
        'from_es' => 'с испанского',
        'from_de' => 'с немецкого',
    ],
    'objects' => [
        'create' => 'Добавить объект'
    ]
];