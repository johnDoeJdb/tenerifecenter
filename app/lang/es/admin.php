<?php
return [
    'login' => [
        'email' => 'Email',
        'password' => 'Contraseña',
        'memorize' => 'Recordar',
        'login' => 'Iniciar sesión',
    ],
    'site' => [
        'title' => 'Administrativo sección',
        'logout' => 'Salida',
        'to_site' => 'Ir al sitio web'

    ],
    'menu' => [
        'objects' => 'Objetos',
        'news' => 'Noticias',
        'pages' => 'Página',
        'tags' => 'Etiquetas',
        'places' => 'Места',
        'complexes' => 'Комплексы',
        'banners' => 'Баннеры',
        'offices' => 'Офисы',
        'managers' => 'Los gerentes'
    ],
    'lang' => [
        'translate' => 'Traducción',
        'ru' => 'Ruso',
        'en' => 'Inglés',
        'es' => 'Español',
        'de' => 'Alemán',
        'from_ru' => 'de ruso',
        'from_en' => 'del inglés',
        'from_es' => 'de español',
        'from_de' => 'de alemán',
    ]
];