<?php

Route::get('/changelog', 'HomeController@changelog');

Route::pattern('{id}', '\d+');

Route::group(['prefix'=> 'admin', 'namespace' => 'Admin'], function() {

    $lang = Request::segment(2);
    if (in_array($lang, Config::get('app.locales'))) {
        App::setLocale($lang);
    }
    else {
        $lang = null;
    }

    Route::group(['prefix'=> $lang], function() {
        Route::any('/login', ['uses' => 'AdminController@login', 'as' => 'admin.login']);

        Route::get('/test', 'AdminController@test');
    });

    Route::group(['prefix'=> $lang, 'before' => 'auth.admin'], function() {

        Route::get('/', ['uses' => 'AdminController@index', 'as' => 'admin.index']);
        Route::get('/logout', ['uses' => 'AdminController@logout', 'as' => 'admin.logout']);

        Route::group(['prefix'=> 'api'], function() {
            Route::post('/translate', 'TranslationController@translate');

            Route::get('/images', 'ImagesController@all');
            Route::post('/images/save', 'ImagesController@save');

            Route::get('/news', 'NewsController@all');
            Route::get('/news/get', 'NewsController@get');
            Route::post('/news/save', 'NewsController@save');

            Route::get('/tags', 'TagsController@all');
            Route::get('/tags/get', 'TagsController@get');
            Route::post('/tags/save', 'TagsController@save');

            Route::get('/pages', 'PagesController@all');
            Route::get('/pages/get', 'PagesController@get');
            Route::post('/pages/save', 'PagesController@save');

            Route::get('/objects/environment', 'ObjectsController@allEnvironment');
            Route::get('/objects', 'ObjectsController@all');
            Route::get('/objects/get/environment', 'ObjectsController@getEnvironment');
            Route::get('/objects/get', 'ObjectsController@get');
            Route::post('/objects/save', 'ObjectsController@save');

            Route::get('/managers/environment', 'ManagersController@getEnvironment');
            Route::get('/managers', 'ManagersController@all');
            Route::get('/managers/get', 'ManagersController@get');
            Route::post('/managers/save', 'ManagersController@save');

            Route::get('/places', 'PlacesController@all');
            Route::get('/places/get', 'PlacesController@get');
            Route::post('/places/save', 'PlacesController@save');

            Route::get('/offices/get/environment', 'OfficesController@getEnvironment');
            Route::get('/offices', 'OfficesController@all');
            Route::get('/offices/get', 'OfficesController@get');
            Route::post('/offices/save', 'OfficesController@save');

            Route::get('/complexes/get/environment', 'ComplexesController@getEnvironment');
            Route::get('/complexes', 'ComplexesController@all');
            Route::get('/complexes/get', 'ComplexesController@get');
            Route::post('/complexes/save', 'ComplexesController@save');
        });
    });
});

$lang = Request::segment(1);

if (in_array($lang, Config::get('app.locales'))) {
    App::setLocale($lang);
}
else {
    $lang = null;
}

Route::group(['prefix' => $lang], function() use (&$lang) {
    Route::get('/', 'HomeController@index');
    Route::get('/sale/{id}', 'HomeController@sale');
    Route::get('/sale/', 'HomeController@salePage');
    Route::get('/rent/{id}', 'HomeController@rent');
    Route::get('/rent/', 'HomeController@rentPage');
    Route::get('/object/{id}', 'HomeController@object');
    Route::get('/about-team', 'HomeController@aboutTeam');
    Route::get('/about-offices', 'HomeController@aboutOffices');
    Route::get('/about-services', 'HomeController@aboutServices');
    Route::get('/about-jobs', 'HomeController@aboutJobs');

    if ($lang) {
        Route::get('/search', 'HomeController@search');

        Route::post('/api/search', 'ApiController@search');
        Route::post('/api/favorites', 'ApiController@favorites');
        Route::get('/api/object/{id}', 'ApiController@object');
    }

    Route::get('/hot/id/{id}', 'ObjectController@show');

    /**
     * Статичные страницы
     */
    Route::get('{path}', 'PageController@show')->where('path', '.+');
});
