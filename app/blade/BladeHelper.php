<?php

namespace Tenerife\Blade;

use Intervention\Image\Facades\Image;

class BladeHelper
{
    public static function imageResize($path, $width, $height)
    {
        $filePath = public_path($path);
        $cachePath = public_path('/cache/'.$path);
        $cacheDirectory = dirname($cachePath);
        @mkdir($cacheDirectory, 0775, true);
        Image::cache(function($image) use ($filePath, $width, $height, $cachePath) {
            return $image->make($filePath)->fit($width, $height)->save($cachePath, 100);
        });

        return '/cache/'.$path;
    }

    public static function getLanguages()
    {
        $languages =
        [
            'en' => 'English',
            'de' => 'Deutsch',
            'es' => 'Español',
            'ru' => 'Русский',
        ];

        return $languages;
    }
}