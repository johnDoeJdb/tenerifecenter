<?php
return [
    'tags' => [
        'type' => 7,
        'main' => 1,
        'sold' => 2,
        'exclusive' => 3,
        'specialPrice' => 4,
        'reducedPrice' => 5,
    ]
];
