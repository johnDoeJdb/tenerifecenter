<?php

return array(

    'fetch' => PDO::FETCH_CLASS,

    'default' => 'mysql',

    'connections' => array(

        'mysql' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'tenerifecenter',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix'    => '',
        ),

    ),

    'migrations' => 'migrations',

);
