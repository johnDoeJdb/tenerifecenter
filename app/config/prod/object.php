<?php
return [
    'tags' => [
  'type' => 7,
  'direct' => 2,
  'main' => 1,
  'sold' => 3,
  'exclusive' => 4,
  'specialPrice' => 5,
  'reducedPrice' => 6,
    ]
];
