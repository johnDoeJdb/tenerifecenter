<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('objects', function(Blueprint $table)
		{
            $table->increments('id');
            foreach (Config::get('app.locales') as $lang) {
                $table->text("name-$lang")->nullable();
                $table->text("teaser-$lang")->nullable();
                $table->text("description-$lang")->nullable();
            }

            // VS*, VR*
            $table->integer('public_id', false, true)->nullable()->unique();

            // rent, sale
            $table->string('for', 4)->default(false);

            $table->integer('price', false, true)->nullable();

            $table->integer('number_bedrooms', false, true)->nullable();
            $table->integer('living_space', false, true)->nullable();
            $table->integer('gross_area', false, true)->nullable();
            $table->integer('build_year', false, true)->nullable();
            $table->integer('floor', false, true)->nullable();
            $table->integer('floors', false, true)->nullable();
            $table->integer('garage_capacity', false, true)->nullable();
            $table->integer('parking_places', false, true)->nullable();
            $table->integer('beach_distance', false, true)->nullable();
            $table->integer('ocean_distance', false, true)->nullable();
            $table->integer('utility_payment', false, true)->nullable();
            $table->integer('annual_tax', false, true)->nullable();
            $table->integer('waste_tax', false, true)->nullable();

            // место, в котором расположен объект
            $table->integer('place_id', false, true)->nullable();
            // менеджер, чей объект
            $table->integer('manager_id', false, true)->nullable();
            // жилой комплекс
            $table->integer('complex_id', false, true)->nullable();

            $table->text('owner_name')->nullable();
            $table->text('owner_email')->nullable();
            $table->text('owner_phone')->nullable();
            $table->text('notes')->nullable();

            $table->string('address', 100)->nullable();
            $table->float('lat', 20, 17)->nullable();
            $table->float('lng', 20, 17)->nullable();

            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('objects');
	}

}
