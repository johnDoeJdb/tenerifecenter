<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsImages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('objects_images', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('image_id', false, true);
            $table->integer('object_id', false, true);
            $table->integer('order', false, true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('objects_images');
	}

}
