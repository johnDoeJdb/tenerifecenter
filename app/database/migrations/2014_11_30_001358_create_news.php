<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
            $table->increments('id');
            foreach (Config::get('app.locales') as $lang) {
                $table->text("name-$lang")->nullable();
                $table->text("teaser-$lang")->nullable();
                $table->text("content-$lang")->nullable();
            }

            $table->integer('published_at', false, true)->nullable();
            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
