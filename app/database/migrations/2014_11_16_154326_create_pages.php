<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('parent_id')->nullable();
            foreach (Config::get('app.locales') as $lang) {
                $table->string("path-$lang", 30)->nullable();
                $table->text("name-$lang")->nullable();
                $table->text("content-$lang")->nullable();
                $table->text("keywords-$lang")->nullable();
                $table->text("description-$lang")->nullable();
                $table->unique(['parent_id', "path-$lang"]);
            }
            $table->integer('created_at');
            $table->integer('updated_at');
            $table->integer('deleted_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
