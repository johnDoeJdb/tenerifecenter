<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offices', function(Blueprint $table)
		{
            $table->increments('id');
            foreach (Config::get('app.locales') as $lang) {
                $table->text("name-$lang")->nullable();
                $table->text("description-$lang")->nullable();
            }

            $table->boolean('show')->default(true);

            $table->integer('place_id', false, true)->nullable();

            $table->string('address', 100)->nullable();
            $table->float('lat', 20, 17)->nullable();
            $table->float('lng', 20, 17)->nullable();

            $table->string('email', 60);
            $table->string('skype', 60)->nullable();
            $table->string('phone', 60)->nullable();
            $table->string('image', 60)->nullable();

            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offices');
	}

}
