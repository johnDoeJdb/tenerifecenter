<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->boolean('active')->default(true);
            $table->boolean('show')->default(true);
            $table->boolean('show_skype')->default(true);
            $table->boolean('show_phone')->default(true);
            $table->boolean('show_email')->default(false);
            $table->boolean('lang_ru')->default(false);
            $table->boolean('lang_en')->default(false);
            $table->boolean('lang_es')->default(false);
            $table->boolean('lang_de')->default(false);
            $table->string('role', 20);
            $table->string('email', 60);
            $table->string('password', 60);
            $table->string('name-ru', 60)->nullable();
            $table->string('name-en', 60)->nullable();
            $table->string('skype', 60)->nullable();
            $table->string('phone', 60)->nullable();
            $table->string('image', 60)->nullable();
            $table->text('description')->nullable();
            $table->rememberToken();

            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
