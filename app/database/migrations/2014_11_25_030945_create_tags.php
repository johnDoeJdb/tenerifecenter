<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tags', function(Blueprint $table)
		{
			$table->increments('id');

            // system, feature
            $table->string('type', 20);

            foreach (Config::get('app.locales') as $lang) {
                $table->text("name-$lang")->nullable();
                $table->text("description-$lang")->nullable();
            }

            // если true, то тег нельзя удалить
            $table->boolean('protected')->default(false);
            // наличие флага говорит о том, что тег является агрегирующим (Вид: горы/море/свалка)
            $table->boolean('group')->default(false);
            // ссылка на название тега. А текущая сущность лишь его значение
            $table->integer('parent_id', false, true)->nullable();

            $table->string('icon_class', 30)->nullable();
            $table->integer('order', false, true)->default(1);
            $table->boolean('rent')->default(true);
            $table->boolean('sale')->default(true);
            $table->boolean('search')->default(true);
            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tags');
	}

}
