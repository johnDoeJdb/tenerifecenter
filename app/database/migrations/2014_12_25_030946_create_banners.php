<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanners extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners', function(Blueprint $table)
		{
            $table->increments('id');

            $table->integer('place', false, true); // 1-6 баннерное место

            $table->string('type', 6); // object / news / image / page
            $table->integer('object_id', false, true);

            $table->integer('created_at', false, true);
            $table->integer('updated_at', false, true);
            $table->integer('deleted_at', false, true)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners');
	}

}
