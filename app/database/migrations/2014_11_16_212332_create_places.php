<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaces extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            foreach (Config::get('app.locales') as $lang) {
                $table->text("name-$lang")->nullable();
                $table->text("description-$lang")->nullable();
            }
            $table->float('lat', 20, 17)->nullable();
            $table->float('lng', 20, 17)->nullable();

            $table->integer('created_at');
            $table->integer('updated_at');
            $table->integer('deleted_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('places');
	}

}
