<?php

class PlacesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('places')->delete();

        $region = Place::create([
            'parent_id' => null,
            'name-ru' => 'Адехе',
            'name-en' => 'Adeje',
            'name-es' => 'Adeje',
            'name-de' => 'Adeje',
            'description-ru' => 'Описание Адехе'
        ]);

        Place::create([
            'parent_id' => $region->id,
            'name-ru' => 'Фанабе',
            'name-en' => 'Fanabe',
            'name-es' => 'Fañabe',
            'name-de' => 'Fañabe',
            'description-ru' => 'Описание Фанабе'
        ]);
        Place::create([
            'parent_id' => $region->id,
            'name-ru' => 'Плая параисо',
            'name-en' => 'Playa Paraiso',
            'name-es' => 'Playa Paraiso',
            'name-de' => 'Playa Paraiso'
        ]);

    }

}
