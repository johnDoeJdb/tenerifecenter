<?php

class TagsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();
        DB::update('alter table tags AUTO_INCREMENT = 1');

        $main = Tag::create([
            'type' => 'system',
            'name-ru' => 'На главной странице'
        ]);

        $direct = Tag::create([
            'type' => 'system',
            'name-ru' => 'Прямой объект'
        ]);

        $sold = Tag::create([
            'type' => 'system',
            'name-ru' => 'Продано'
        ]);

        $exclusive = Tag::create([
            'type' => 'system',
            'name-ru' => 'Эксклюзив'
        ]);

        $special_price = Tag::create([
            'type' => 'system',
            'name-ru' => 'Специальная цена'
        ]);

        $reduced_price = Tag::create([
            'type' => 'system',
            'name-ru' => 'Цена снижена'
        ]);



        $type = Tag::create([
            'protected' => true,
            'type' => 'feature',
            'name-ru' => 'Тип недвижимости',
            'name-en' => 'Property type',
            'name-es' => 'Tipo de inmueble',
            'name-de' => 'Art der Immobilie',
            'group' => true
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Вилла',
            'name-en' => 'Villa',
            'name-es' => 'Villa',
            'name-de' => 'Villa',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Таунхаус',
            'name-en' => 'Townhouse',
            'name-es' => 'Adosado',
            'name-de' => 'Reihenhaus',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Квартира',
            'name-en' => 'Apartment',
            'name-es' => 'Apartamento',
            'name-de' => 'Wohnung',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Пентхаус',
            'name-en' => 'Penthouse',
            'name-es' => 'Ático',
            'name-de' => 'Penthouse',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Дуплекс',
            'name-en' => 'Duplex',
            'name-es' => 'Duplex',
            'name-de' => 'Duplex',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Бунгало',
            'name-en' => 'Bungalow',
            'name-es' => 'Bungalow',
            'name-de' => 'Bungalow',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Студия',
            'name-en' => 'Studio',
            'name-es' => 'Estudio',
            'name-de' => 'Studio',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Дом',
            'name-en' => 'House',
            'name-es' => 'Casa',
            'name-de' => 'Haus',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Поместье',
            'name-en' => 'Manor',
            'name-es' => 'Finca',
            'name-de' => 'Anwesen',
        ]);

        Tag::create([
            'type' => 'feature',
            'parent_id' => $type->id,
            'name-ru' => 'Земля',
            'name-en' => 'Land',
            'name-es' => 'Tierra',
            'name-de' => 'Die Erde',
        ]);


        $br = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Ванные комнаты',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $br->id,
            'type' => 'feature',
            'name-ru' => 'Ванные комнаты suite'
        ]);

        Tag::create([
            'parent_id' => $br->id,
            'type' => 'feature',
            'name-ru' => 'Душевая кабина'
        ]);

        Tag::create([
            'parent_id' => $br->id,
            'type' => 'feature',
            'name-ru' => 'Джакузи'
        ]);


        $kitchen = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Кухня',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $kitchen->id,
            'type' => 'feature',
            'name-ru' => 'Отдельная'
        ]);

        Tag::create([
            'parent_id' => $kitchen->id,
            'type' => 'feature',
            'name-ru' => 'Американского типа'
        ]);


        $quality = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Состояние',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $quality->id,
            'type' => 'feature',
            'name-ru' => 'Новый'
        ]);

        Tag::create([
            'parent_id' => $quality->id,
            'type' => 'feature',
            'name-ru' => 'Хорошее'
        ]);

        Tag::create([
            'parent_id' => $quality->id,
            'type' => 'feature',
            'name-ru' => 'Нормальное'
        ]);


        $furniture = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Мебелирован',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $furniture->id,
            'type' => 'feature',
            'name-ru' => 'Полностью мебелирован'
        ]);

        Tag::create([
            'parent_id' => $furniture->id,
            'type' => 'feature',
            'name-ru' => 'Частично мебелирован'
        ]);

        Tag::create([
            'parent_id' => $furniture->id,
            'type' => 'feature',
            'name-ru' => 'Без мебели'
        ]);


        $view = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Вид',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $view->id,
            'type' => 'feature',
            'name-ru' => 'На бассейн'
        ]);

        Tag::create([
            'parent_id' => $view->id,
            'type' => 'feature',
            'name-ru' => 'На горы'
        ]);

        Tag::create([
            'parent_id' => $view->id,
            'type' => 'feature',
            'name-ru' => 'На океан'
        ]);

        Tag::create([
            'parent_id' => $view->id,
            'type' => 'feature',
            'name-ru' => 'На улицу'
        ]);


        $garden = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Сад',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $garden->id,
            'type' => 'feature',
            'name-ru' => 'Общий'
        ]);

        Tag::create([
            'parent_id' => $garden->id,
            'type' => 'feature',
            'name-ru' => 'Частный'
        ]);


        $terrace = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Терасса',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $terrace->id,
            'type' => 'feature',
            'name-ru' => 'На бассейн'
        ]);

        Tag::create([
            'parent_id' => $terrace->id,
            'type' => 'feature',
            'name-ru' => 'На горы'
        ]);

        Tag::create([
            'parent_id' => $terrace->id,
            'type' => 'feature',
            'name-ru' => 'На океан'
        ]);

        Tag::create([
            'parent_id' => $terrace->id,
            'type' => 'feature',
            'name-ru' => 'Нет вида'
        ]);


        $balcony = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Балкон',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $balcony->id,
            'type' => 'feature',
            'name-ru' => 'На бассейн'
        ]);

        Tag::create([
            'parent_id' => $balcony->id,
            'type' => 'feature',
            'name-ru' => 'На горы'
        ]);

        Tag::create([
            'parent_id' => $balcony->id,
            'type' => 'feature',
            'name-ru' => 'На океан'
        ]);

        Tag::create([
            'parent_id' => $balcony->id,
            'type' => 'feature',
            'name-ru' => 'Нет вида'
        ]);

        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Соляриум'
        ]);


        $garage = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Гараж',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $garage->id,
            'type' => 'feature',
            'name-ru' => 'Общий'
        ]);

        Tag::create([
            'parent_id' => $garage->id,
            'type' => 'feature',
            'name-ru' => 'Частный'
        ]);


        $sp = Tag::create([
            'type' => 'feature',
            'name-ru' => 'Бассейн',
            'group' => true
        ]);

        Tag::create([
            'parent_id' => $sp->id,
            'type' => 'feature',
            'name-ru' => 'Общий'
        ]);

        Tag::create([
            'parent_id' => $sp->id,
            'type' => 'feature',
            'name-ru' => 'Частный'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Бассейн с подогревом'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Детский бассейн'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Кондиционер'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Солнечные  батареи'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Лифт'
        ]);


        Tag::create([
            'type' => 'feature',
            'name-ru' => 'Спортивный зал'
        ]);





        var_export([
            'type' => $type->id,
            'direct' => $direct->id,
            'main' => $main->id,
            'sold' => $sold->id,
            'exclusive' => $exclusive->id,
            'specialPrice' => $special_price->id,
            'reducedPrice' => $reduced_price->id,
        ]);
    }

}
