<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PagesTableSeeder');
		$this->call('TagsTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('ObjectsTableSeeder');
		$this->call('PlacesTableSeeder');
	}

}
