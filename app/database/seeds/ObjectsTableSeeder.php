<?php

class ObjectsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return;

        DB::table('objects')->delete();

        for ($i = 1; $i < 2000; $i++) {
            $price = mt_rand(80, 300) * 1000;
            Object::create([
                'name-ru' => 'Объект '.$i,
                'price' => $price,
                'lat' => mt_rand(2796650784, 2860502008) / 100000000,
                'lng' => mt_rand(-1703157663, -1604555368) / 100000000,
            ]);
        }

    }

}

/*
 * new google.maps.LatLng(27.96650784, -17.03157663),
                            new google.maps.LatLng(28.60502008, -16.04555368)
 */