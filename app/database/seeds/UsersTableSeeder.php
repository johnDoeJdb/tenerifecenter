<?php

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'role' => 'admin',
            'name-ru' => 'Антон Исайкин',
            'email' => 'isaykin@gmail.com',
            'password' => Hash::make('1234')
        ]);

        User::create([
            'role' => 'admin',
            'name-ru' => 'Маргарита',
            'email' => 'm@tenerifecenter.com',
            'password' => Hash::make('1234')
        ]);

        User::create([
            'role' => 'manager',
            'name-ru' => 'Стас Михайлов',
            'email' => '2@2.ru',
            'password' => Hash::make('1234')
        ]);

    }

}
