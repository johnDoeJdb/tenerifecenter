<?php

class PagesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->delete();

        $main = Page::create([
            'parent_id' => null,
            'path-en' => '',
            'path-ru' => '',
            'path-es' => '',
            'path-de' => '',
            'name-en' => 'Main',
            'name-ru' => 'Главная',
            'name-es' => 'Home Page',
            'name-de' => 'Startseite',
        ]);

        $first = Page::create([
            'parent_id' => $main->id,
            'path-en' => 'luxury',
            'path-ru' => 'luxury',
            'path-es' => 'luxury',
            'path-de' => 'luxury',
            'name-en' => 'Luxury',
            'name-ru' => 'Элитная недвижимость',
            'name-es' => 'Lujo',
            'name-de' => 'Luxury',
        ]);

        Page::create([
            'parent_id' => $first->id,
            'path-en' => 'luxury2',
            'path-ru' => 'luxury2',
            'path-es' => 'luxury2',
            'path-de' => 'luxury2',
            'name-en' => 'Luxury 2',
            'name-ru' => 'Элитная недвижимость 2',
            'name-es' => 'Lujo 2',
            'name-de' => 'Luxury 2',
        ]);
    }

}
