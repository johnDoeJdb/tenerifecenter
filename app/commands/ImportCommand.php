<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:objects';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import objects from http://www.tenerifecenter.com/realty.xml';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$googleApiKey = 'AIzaSyBvLreZ_g2lvlwWNkF97zgDnVxdDcYzUy4';
		$yandexApiKey = 'trnsl.1.1.20150818T172310Z.11f74b540de50cd3.04b93d83603098cb6371df1852de473fe5d35933';
//		$client = new \GuzzleHttp\Client();
//		$response = $client->get('http://www.tenerifecenter.com/realty.xml');
//		$xml = $response->getBody();
		$path = realpath(app_path().'\..').'/objects.xml';
//		file_put_contents($path, $xml);
		$xml = simplexml_load_file($path);

		$insertPlace = function($googleApiKey, $yandexApiKey, $address) {
			if (!$address) {
				return null;
			}

			$results = DB::select('select * from places where `name-en` = ?', array($address));
			if ($results) {
				return $results[0]->id;
			}

			$client = new \GuzzleHttp\Client();
			$response = $client->get('https://translate.yandex.net/api/v1.5/tr.json/translate?key='.$yandexApiKey.'&lang=en-ru&text='.$address);
			$nameRu = json_decode($response->getBody());
			$name['ru'] = ($nameRu->text[0]);

			$response = $client->get('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.',+Spain&key='.$googleApiKey);
			$geoAddressJson = $response->getBody();
			$geoAddress = json_decode($geoAddressJson);
			if (!$geoAddress->results) {
				return null;
			}
			$location = $geoAddress->results[0]->geometry->location;
			$latitude = $location->lat;
			$longitude = $location->lng;
			$timestamp = time();

			$id = DB::table('places')->insertGetId([
				[
					'name-ru' => $name['ru'],
					'name-en' => $address,
					'lat' => $latitude,
					'lng' => $longitude,
					'created_at' => $timestamp,
					'updated_at' => $timestamp,
				],
			]);

			return $id;
		};

		$getType = function(
			$typeOld
		) {
			switch ($typeOld) {
				case 'b':
					$type = 'Вилла';
					break;
				case 'd':
					$type = 'Апартаменты';
					break;
				case 'e':
					$type = 'Таунхаус';
					break;
				case 'j':
					$type = 'Коммерческое помещение';
					break;
				case 'k':
					$type = 'Вилла';
					break;
				case 'o':
					$type = 'Земля';
					break;
				case 'p':
					$type = 'Бунгало';
					break;
				case 'r':
					$type = 'Коммерческое помещение';
					break;
				case 't':
					$type = 'Коммерческое помещение';
					break;
				case 'g':
					$type = 'Пентхаус';
					break;
				case 'w':
					$type = 'Коммерческое помещение';
					break;
			}
			$tag = DB::table('tags')->where('name-ru', $type)->first();
			if (!$tag) {
				return null;
			}

			return $tag->id;
		};


		$insertObject = function(
			$googleApiKey,
			$yandexApiKey,
			$address,
			$purpose,
			$shortDescription,
			$fullDescription,
			$totalArea,
			$bedrooms,
			$furnished,
			$priceSell,
			$priceRent,
			$placeId,
			$tagId,
			$publicId
		) {
//			$client = new \GuzzleHttp\Client();
			$for = $purpose == 'r' ? 'rent' : 'sale';
//			preg_match("/^(.*?)[.,«\"!?']/", $shortDescription, $matches);
//			$name['ru'] = $matches[1];
			$description['ru'] = $fullDescription ? $fullDescription : $shortDescription;
			$teaser['ru'] = $furnished;
			$timestamp = time();
			$price = $purpose == 'r' ? $priceRent : $priceSell;
			$place = DB::table('places')->where('id', $placeId)->first();
			$tag = DB::table('tags')->where('id', $tagId)->first();
			$name['ru'] = $tag->{'name-ru'}.' в '.$place->{'name-ru'};

			$objectId = DB::table('objects')->insertGetId([
				'name-ru' => $name['ru'],
				'teaser-ru' => $teaser['ru'],
				'description-ru' => $description['ru'],
				'for' => $for,
				'number_bedrooms' => $bedrooms,
				'gross_area' => $totalArea,
				'created_at' => $timestamp,
				'place_id' => $placeId,
				'price' => $price,
				'public_id' => $publicId,
			]);

			$this->info('Object id: '.$objectId."\r\n");

			return $objectId;
		};

		$insertObjectTag = function($objectId, $tagId) {
			$results = DB::table('objects_tags')->where('tag_id', $tagId)->where('object_id', $objectId)->first();
				if ($results) {
				return null;
			}
			DB::table('objects_tags')->insert([
				[
					'tag_id' => $tagId,
					'object_id' => $objectId,
				],
			]);
			DB::table('objects_tags')->insert([
				[
					'tag_id' => 1,
					'object_id' => $objectId,
				],
			]);
		};

		$insertObjectImages = function($url, $objectId, $order) {
			preg_match('/.*?\/(\d+)\.(\w+)$/', $url, $matches);
			$fileName = $matches[1];
			$extension = $matches[2];
//			$fullName = $fileName.'.'.$extension;
			if (!in_array($extension, ['jpg', 'png'])) {
				return;
			}
			$results = DB::table('images_excluded')->where('name', $fileName)->first();
			if ($results) {
				return;
			}
//			if (in_array($fullName,
//				['9.gif','10.gif','11.gif','12.gif','308.jpg','775.jpg','1264.jpg','1265.jpg','1266.jpg','1267.jpg','1268.jpg','1509.jpg','1510.jpg','2342.jpg','2343.jpg','2344.jpg','2345.jpg','2346.jpg','4213.jpg','4479.jpg','4715.gif','4729.jpg','5263.jpg','5821.jpg','6426.jpg','8106.jpg','8289.gif','8290.gif','8291.jpg','9064.jpg','9092.jpeg','9093.jpeg','9094.jpeg','9095.jpeg','9096.jpeg','9595.jpg','9596.jpg','9597.jpg','9599.jpg','9600.jpg','9601.jpg','9863.jpg','9962.jpg','10011.jpg','10012.pdf','10102.jpg','10434.jpg','10456.jpg','10958.jpg','10959.jpg','10960.png','10963.jpg','10965.jpg','11753.jpg','11789.jpg','11790.jpg','13954.jpg','13959.jpg','13960.png','14088.png','14411.png','14690.jpg','14795.png','15565.jpg','15602.jpeg','15603.jpeg','15604.png','15606.png','15607.jpg','15608.jpg','15609.jpg','15610.jpg','15662.swf','15739.','15744.jpg','15745.jpg','15746.png','15747.png','15748.jpg','15749.jpg','15750.jpg','15751.gif','15752.jpg','15753.jpeg','15754.jpg','15755.jpg','15756.jpg','15757.jpg','15758.jpg','15759.jpg','15869.gif','15875.jpg','15876.png','15877.gif','16563.jpg','16564.jpg','16565.jpg','16635.jpg','16640.jpg','16865.png','16866.png','17307.png','17356.gif','17980.jpg','18110.jpg','18569.jpg','18894.jpg','18895.jpg','18896.jpg','18897.jpg','18898.jpg','18899.jpg','18900.jpg','18901.jpg','19212.png','19213.png','19214.png','19215.png','20370.jpg','20374.jpg'])) {
//				return;
//			} else {
//
//			}

			try {
				$fullsize = uniqid().'.'.$extension;
				$preview = uniqid().'.'.$extension;

				$imageFile = file_get_contents($url);
				$img = Intervention::make($imageFile);
				$img->resize(1024, null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$img->insert(public_path('assets/images/watermark.png'), 'bottom-right', 10, 10);
				$img->save(\Config::get('app.images.objects').'/'.$fullsize);

				$img = Intervention::make($imageFile);
				$img->resize(200, null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$img->save(\Config::get('app.images.objects').'/'.$preview);

			} catch (\Exception $e) {
				return;
			}

			$image = new Image();
			$image->fill([
				'preview' => $preview,
				'fullsize' => $fullsize
			]);
			$image->save();

			$id = DB::table('objects_images')->insertGetId([
				'image_id' => $image->id,
				'object_id' => $objectId,
				'order' => $order,
			]);

			DB::table('images_excluded')->insert([
				'name' => $fileName,
			]);

			return $id;
		};

		foreach($xml as $object) {
			try {
				$results = DB::table('counters')->where('last_parsed_id', (integer) $object->id)->first();
				if ($results) {
					return;
				}
				$placeId = $insertPlace($googleApiKey, $yandexApiKey, (string) $object->address);
				$tagId = $getType((string) $object->type);
				$objectId = $insertObject(
					$googleApiKey,
					$yandexApiKey,
					(string) $object->address,
					(string) $object->purpose,
					(string) $object->description_short,
					(string) $object->description_full,
					(integer) $object->total_area,
					(integer) $object->bedrooms,
					(string) $object->furnished,
					(integer) $object->price_sell,
					(integer) $object->price_rent,
					$placeId,
					$tagId,
					(integer) $object->id
				);
				$insertObjectTag($objectId, $tagId);
				$counter = 1;
				foreach ((array) $object->photos->url as $photo) {
					$objectImageId = $insertObjectImages($photo, $objectId, $counter);
					$objectImageId ? $counter++ : null;
				}
				DB::table('counters')->insert([
					'last_parsed_id' => (integer) $object->id,
				]);
			} catch (\Exception $e) {
				continue;
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}



}
