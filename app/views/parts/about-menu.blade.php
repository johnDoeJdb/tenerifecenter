<div class="b-menu-about">
    <a class="e-nav__about @if (Request::path() == 'about-team') js-e-main-menu-current @endif" href="/about-team">Our Team</a>
    <a class="e-nav__about @if (Request::path() == 'about-offices') js-e-main-menu-current @endif" href="/about-offices">Offices</a>
    <a class="e-nav__about @if (Request::path() == 'about-services') js-e-main-menu-current @endif" href="/about-services">Services</a>
    <a class="e-nav__about @if (Request::path() == 'about-jobs') js-e-main-menu-current @endif" href="/about-jobs">Jobs</a>
</div>