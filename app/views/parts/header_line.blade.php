<nav class="navbar navbar-default navbar-static-top header-line" role="navigation" style="margin-bottom: 0;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/{{ App::getLocale() }}">Tenerifecenter</a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @foreach (Config::get('app.locales') as $lang)
                    <li class="{{ Config::get('app.locale') == $lang ? 'active' : '' }}">
                        <a href="/{{ $lang }}"><img src="/assets/images/flags/{{ $lang }}.png" title="{{ trans('site.langs.'.$lang) }}" /></a>
                    </li>
                @endforeach
                @if (!Auth::guest())
                    <li>
                        <a href="/admin" style="font-weight: bold;">Админ</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>