<style>
    .splash {height: 260px; background-image: url(/assets/images/header-background.jpg); background-position: center;}
    .form-search {margin-top: 40px; padding: 10px 0; background-color: lightgrey; border-radius: 10px; border: 1px solid darkgray;}
    .form-search .row {margin: 12px 0;}
</style>

<div class="container-fluid ng-cloak" ng-app="Search" ng-controller="SearchController">

    <div class="row splash">
        <div class="col-sm-offset-2 col-sm-4 form-search" ng-init="
            locale = '{{ App::getLocale() }}';
            types = {{{ json_encode($types, JSON_UNESCAPED_UNICODE) }}};
            places = {{{ json_encode($places, JSON_UNESCAPED_UNICODE) }}};
            search.for = 'sale';
            run()
        ">
            <form action="/{{ App::getLocale() }}/search" method="get">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="radio-inline">
                            <input type="radio" name="for" value="sale" ng-model="search.for" ng-change="search.price_from = null; search.price_to = null; updatePrices()"> {{ trans('site.search.sale') }}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="for" value="rent" ng-model="search.for" ng-change="search.price_from = null; search.price_to = null; updatePrices()"> {{ trans('site.search.rent') }}
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <select name="type" class="form-control" ng-model="search.type" ng-options="type as type.name for type in types track by type.id">
                            <option value="">{{ trans('site.search.type') }}</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <select name="place" class="form-control" ng-model="search.place" ng-options="place as place.name group by place.region for place in places track by place.id">
                            <option value="">{{ trans('site.search.place') }}</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <select name="price_from" class="form-control" ng-model="search.price_from" ng-options="price as formatPrice(price) for price in options.prices_from track by price" ng-change="updatePrices()">
                            <option value="">{{ trans('site.search.price_from') }}</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="price_to" class="form-control" ng-model="search.price_to" ng-options="price as formatPrice(price) for price in options.prices_to track by price" ng-change="updatePrices()">
                            <option value="">{{ trans('site.search.price_to') }}</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="bedrooms" class="form-control" ng-model="search.bedrooms">
                            <option value="">{{ trans('site.search.bedrooms') }}</option>
                            @for ($i = 1; $i <= 10; $i++)
                                <option value="{{ $i }}">{{ trans('site.search.from') }} {{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="code" class="form-control" placeholder="{{ trans('site.search.code') }}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-9">
                        <input type="text" name="query" class="form-control" placeholder="{{ trans('site.search.keywords') }}">
                    </div>
                    <div class="col-sm-3">
                        <input type="submit" class="btn btn-primary btn-block" value="{{ trans('site.search.search') }}">
                    </div>
                </div>
            </form>



        </div>
    </div>

</div>

<script>
    angular
        .module('Search', [])
        .controller('SearchController', ['$scope', function($scope) {
            $scope.search = {};
            $scope.options = {};

            var getPrices = function(side) {
                var from = side == 'from';
                var result = [];
                var numbers = {
                    sale: {
                        min: 10000,
                        max: 20000000,
                        steps: [
                            [100000, 5000],
                            [300000, 20000],
                            [600000, 50000],
                            [1000000, 100000],
                            [null, 500000]
                        ]
                    },
                    rent: {
                        min: 250,
                        max: 5000,
                        steps: [
                            [1000, 50],
                            [1500, 100],
                            [2000, 250],
                            [3000, 500],
                            [null, 1000]
                        ]
                    }
                };
                for (var i = numbers[$scope.search.for].min; i < numbers[$scope.search.for].max;) {
                    if (i < numbers[$scope.search.for].steps[0][0]) {
                        i += numbers[$scope.search.for].steps[0][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[1][0]) {
                        i += numbers[$scope.search.for].steps[1][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[2][0]) {
                        i += numbers[$scope.search.for].steps[2][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[3][0]) {
                        i += numbers[$scope.search.for].steps[3][1];
                    }
                    else {
                        i += numbers[$scope.search.for].steps[4][1];
                    }
                    if (from && $scope.search.price_to && $scope.search.price_to <= i) {
                        break;
                    }
                    else if (!from && $scope.search.price_from && $scope.search.price_from >= i) {
                        continue;
                    }
                    result.push(i);
                }
                return result;
            };

            $scope.formatPrice = function(price) {
                return '€ ' + price.format(0, 3, ' ');
            };

            $scope.updatePrices = function() {
                $scope.options.prices_from = getPrices('from');
                $scope.options.prices_to = getPrices('to');
            };

            $scope.run = function() {
                $scope.updatePrices();
            }
        }])
</script>