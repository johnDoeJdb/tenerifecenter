<div class="l-offers">
    <div class="b-head__offers">
        <div class="e-title__offers">{{Lang::get('site.offers.offers')}}</div>
        <div class="b-offer-nav__choose">
            <input class="e-radio__offer" type="radio" id="latest" value="latest" name="offer-filter"><label ng-click="setOrderById()" for="latest" class="e-offer-choose">{{Lang::get('site.offers.latest')}}</label>
            <input class="e-radio__offer" type="radio" id="lowest" value="lowest" name="offer-filter"><label ng-click="setOrderByPrice()" for="lowest" class="e-offer-choose">{{Lang::get('site.offers.lowest_price')}}</label>
        </div>
    </div>
    <div class="b-offer-previews">
        <span ng-repeat="object in objects | orderBy: orderType | limitTo: limitOffers">
            <a class="b-offer-preview" href="/ng{object['for']}ng/ng{object['id']}ng">
                <img class="b-offer-photo" alt=""  src="ng{ object['image'] }ng">
                <div class="b-offer-preview__desc">
                    <div class="e-offer-code">Code VS2569</div>
                    <div class="e-offer-name">ng{object['name']}ng</div>
                </div>
                <div class="b-offer-chars">
                    <div class="b-meters__chars">
                        <div class="e-offer-char-name">{{Lang::get('site.offers.sq_meters')}}</div>
                        <div class="e-offer-char-value">ng{object['living_space'] ? object['living_space'] : '-' }ng</div>
                    </div>
                    <div class="b-rooms-chars">
                        <div class="e-offer-char-name">{{Lang::get('site.offers.rooms')}}</div>
                        <div class="e-offer-char-value">ng{object['number_bedrooms'] ? object['number_bedrooms'] : '-' }ng</div>
                    </div>
                    <div class="b-price-chars">
                        <div class="e-offer-char-name">{{Lang::get('site.offers.price')}}</div>
                        <div class="e-offer-char-value">ng{object['price'] ? getFormatPrice(object['price'])+'&nbsp; €' : '-' }ng</div>
                    </div>
                </div>
            </a>
        </span>
        <div class="b-offer-button"><a ng-click="moreOffers()" onclick="javascript:void(0)" class="e-offer-button" href="">{{Lang::get('site.common.more')}}</a></div>
    </div>
</div>