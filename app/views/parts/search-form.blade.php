{{ Form::open(array('url' => Lang::getLocale().'/search', 'method' => 'get')) }}
        <div class="b-region__search">
                <div class="e-choose-title">{{Lang::get('site.search.region')}}</div>
                <div class="e-choose-value">
                        {{ Form::select('place', $places, null, array('data-className' => 'js-e-choose-value', 'class' => 'js-e-choose-value')); }}
                </div>
        </div>
        <div class="b-complex__search">
                <div class="e-choose-title">{{Lang::get('site.search.complex')}}</div>
                <div class="e-choose-value">
                        {{ Form::select('complex', $complexes, null, array('data-className' => 'js-e-choose-value', 'class' => 'js-e-choose-value')); }}
                </div>
        </div>
        <div class="b-property__search">
                <div class="e-choose-title">{{Lang::get('site.search.property_type')}}</div>
                <div class="e-choose-value">
                        {{ Form::select('type', $propertyTypes, null, array('data-className' => 'js-e-choose-value', 'class' => 'js-e-choose-value')); }}
                </div>
        </div>
        <div class="b-bedrooms__search">
                <div class="e-choose-title">{{Lang::get('site.search.bedrooms')}}</div>
                <div class="e-choose-value">
                        {{ Form::select('bedrooms', [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], null, array('data-className' => 'js-e-choose-value', 'class' => 'js-e-choose-value')); }}
                </div>
        </div>
<div class="b-search__search" >
        <div class="b-search-action">
                <select name="type_deal" class="e-search-action">
                        <option value="rent" onclick="period('block')">{{Lang::get('site.search.rent')}}</option>
                        <option value="sale" onclick="period('none')">{{Lang::get('site.search.sale')}}</option>
                </select>
        </div>
        <div class="b-time__search">
                {{ Form::select('payments_type', ['Per month' => Lang::get('site.search.per_month'), 'Per day' => Lang::get('site.search.per_day')], null, array('class' => 'e-time__search')); }}
        </div>
        {{ Form::text('from', null, array('placeholder' => Lang::get('site.search.free_from'), 'class' => 'e-period-from__search', 'name' => 'calender')); }}
        {{ Form::text('to', null, array('placeholder' => Lang::get('site.search.to'), 'class' => 'e-period-to__search', 'name' => 'calender')); }}
        {{ Form::text('keyword', null, array('placeholder' => Lang::get('site.search.keyword_code'),'class' => 'e-keyword__search')); }}
        {{ Form::submit(Lang::get('site.search.search'), array('class' => 'e-button__search')); }}
</div>
{{ Form::token() }}
{{ Form::close() }}


