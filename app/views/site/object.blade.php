<h1>
    <span style="color: gray;">{{ $object['for'] == 'sale' ? 'VS' : 'VR' }}{{ sprintf('%04d', $object['public_id']) }}</span>
    {{ Object::getLocal($object, 'name')  }}
</h1>

{{ Object::getLocal($object, 'description')  }}

@if ($object->lat)
    <p><img class="img-rounded" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $object->lat }},{{ $object->lng }}&zoom=17&markers={{ $object->lat }},{{ $object->lng }}&size=500x200"></p>
    @if ($object->address) <p>Адрес: {{ $object->address }}</p> @endif
@elseif ($object->complex)
    <p><img class="img-rounded" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $object->complex->lat }},{{ $object->complex->lng }}&zoom=17&markers={{ $object->complex->lat }},{{ $object->complex->lng }}&size=500x200"></p>
    @if ($object->complex->address) <p>Адрес: {{ $object->complex->address }}</p> @endif
@elseif($object->place)
    <p><img class="img-rounded" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $object->place->lat }},{{ $object->place->lng }}&zoom=17&size=500x200"></p>
@endif

@if ($object->place && $object->place->description)
    <p>{{ $object->place->description }}</p>
@elseif ($object->complex && $object->complex->place)
    <p>{{ $object->complex->place->description }}</p>
@endif

<table class="table table-striped" style="max-width: 500px;">
    @if ($object['price']) <tr><td>Цена</td><td>{{ number_format($object['price'], 0, '.', ' ') }} €</td></tr> @endif
    @if ($object['number_bedrooms']) <tr><td>Число спален</td><td>{{ $object['number_bedrooms'] }}</td></tr> @endif
    @if ($object['beach_distance']) <tr><td>До пляжа</td><td>{{ $object['beach_distance'] }} м</td></tr> @endif
    @if ($object['ocean_distance']) <tr><td>До океана</td><td>{{ $object['ocean_distance'] }} м</td></tr> @endif
    @if ($object['gross_area']) <tr><td>Общая площадь</td><td>{{ $object['gross_area'] }} м<sup>2</sup></td></tr> @endif
    @if ($object['living_space']) <tr><td>Жилая площадь</td><td>{{ $object['living_space'] }} м<sup>2</sup></td></tr> @endif
    @if ($object['floor']) <tr><td>Этаж</td><td>{{ $object['floor'] }} @if ($object['floors']) из {{ $object['floors'] }}@endif</td></tr> @endif
    @if ($object['build_year']) <tr><td>Год постройки</td><td>{{ $object['build_year'] }}</td></tr> @endif
    @if ($object['garage_capacity']) <tr><td>Вместимость гаража</td><td>{{ $object['garage_capacity'] }} мест</td></tr> @endif
    @if ($object['parking_places']) <tr><td>Вместимость паркинга</td><td>{{ $object['parking_places'] }} мест</td></tr> @endif
    @if ($object['utility_payment']) <tr><td>Коммунальный платеж</td><td>{{ $object['utility_payment'] }} € / мес</td></tr> @endif
    @if ($object['annual_tax']) <tr><td>Налог</td><td>{{ $object['annual_tax'] }} € / год</td></tr> @endif
    @if ($object['waste_tax']) <tr><td>Налог на мусор</td><td>{{ $object['waste_tax'] }} € / год</td></tr> @endif
</table>

<div class="row">
    @foreach ($object->images as $image)
    <div class="col-md-2">
        <a href="/images/objects/{{ $image['fullsize'] }}" target="_blank" class="thumbnail">
            <img src="/images/objects/{{ $image['preview'] }}" />
        </a>
    </div>
    @endforeach
</div>

<ul>
    @foreach ($object->tags as $tag)
        <li>
            @if ($tag->parent)
                {{ $tag->parent['name'] }}:
            @endif

            <b>{{ $tag['name'] }}</b>
            {{ $tag['description'] }}
        </li>
    @endforeach
</ul>

<h3>Забронировать объект</h3>

<form class="form-horizontal" style="max-width: 500px;">
    <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Телефон</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Сообщение</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Отправить</button>
        </div>
    </div>
</form>