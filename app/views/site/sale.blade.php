@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div ng-init="
        objects = '{{ base64_encode(json_encode($objects, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK)) }}';
        objects = $root.unpackValue(objects);
        "
    >
    <div class="l-category">
        <div class="b-category__category">
            <a class="e-cat__category" href="">Общая база</a>
            <a class="e-cat__category" href="">Новострой</a>
            <a class="e-cat__category" href="">Вторичное жилье</a>
            <a class="e-cat__category" href="">Элитное</a>
            <a class="e-cat__category" href="">Спецпредложения</a>
            <a class="e-cat__category" href="">Банковская</a>
            <a class="e-cat__category" href="">Инвестиции</a>
            <a class="e-cat__category" href="">Снижена цена</a>
            <a class="e-cat__category" href="">Последние поступления</a>
        </div>
    </div>
    <div class="l-search">
        @include('parts.search-form')
    </div>
    <div class="l-form">
        <div class="b-form-info">
            <div class="b-form-info__col1">
                <span style="display: none" {{ $imagesCount = count($images) ? true : false }}></span>
                <img class="e-form-photo" src="{{\Tenerife\Blade\BladeHelper::imageResize('/images/objects/'.$images[0]['fullsize'], 698, 523) }}" alt="">
                <div class="b-form-photos">
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel">
                            <ul>
                                @foreach($images as $image) {
                                    <li><img preload="auto" src="{{\Tenerife\Blade\BladeHelper::imageResize('/images/objects/'.$image['fullsize'], 248, 186) }}" alt="Image 1" onclick="changeImage('{{\Tenerife\Blade\BladeHelper::imageResize('/images/objects/'.$image['fullsize'], 698, 523) }}')"></li>
                                @endforeach
                            </ul>
                        </div>
                        <a href="#" class="jcarousel-control-prev"></a>
                        <a href="#" class="jcarousel-control-next"></a>
                    </div>
                </div>
                <div class="b-form-count">
                    <span class="e-form-code">Code: VS2593</span>
                    <span class="e-form-count">{{number_format($object['price'], 0, ',', ' ')}} €</span>
                </div>
                <div class="e-form-desc">{{ $object['description-ru'] }}</div>
            </div>
            <div class="b-form-info__col2">
                <div class="b-form-subj">{{ $object['name-ru'] }}</div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Property type:</div>
                    <div class="e-form-parm__value">{{ $propertyType['name-ru'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Location:</div>
                    <div class="e-form-parm__value">{{ $place['name-ru'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Living space:</div>
                    <div class="e-form-parm__value">{{ $object['living_space'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Total area:</div>
                    <div class="e-form-parm__value">{{ $object['gross_area'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Number of bedrooms:</div>
                    <div class="e-form-parm__value">{{ $object['number_bedrooms'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Pool:</div>
                    <div class="e-form-parm__value">{{$pool['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Garage/Parking:</div>
                    <div class="e-form-parm__value">{{$garage['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">View:</div>
                    <div class="e-form-parm__value">{{$view['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Furniture:</div>
                    <div class="e-form-parm__value">{{$furniture['name-ru']}}</div>
                </div>
                <div class="b-form-contacts">
                    @if ($manager)
                        <div class="e-form-contacts__title">Manager contacts:</div>
                        @if ($manager['skype'])
                            <div class="b-skype__contact">
                                <img class="e-icon__skype" alt="" src="/includes/e-icon__skype.png">
                                <div class="e-login__skype">{{ $manager ? $manager['skype'] : '' }}</div>
                            </div>
                        @endif
                        @if ($manager['phone'])
                            <div class="b-phone__contact">
                                <img class="e-icon__phone" alt="" src="/includes/e-icon__phone.png">
                                <div class="e-form-contact__info">{{ $manager ? $manager['phone'] : '' }}</div>
                            </div>
                        @endif
                    @endif
                </div>
                <div class="b-pay__rent">
                    <img class="e-visa__pay" alt="" src="/includes/e-visa.png">
                    <img class="e-mcart__pay" alt="" src="/includes/e-mcart.png">
                    <img class="e-amex__pay" alt="" src="/includes/e-amex.png">
                    <img class="e-discover__pay" alt="" src="/includes/e-discover.png">
                    <img class="e-paypal__pay" alt="" src="/includes/e-paypal.png">
                </div>
            </div>
            <div class="b-button__rent">
                <a class="e-checkout__button" href="">Checkout now</a>
            </div>
        </div>
    </div>
    <div class="l-app__rent">
        <form class="b-form__app-rent" name="form" method="post" action="">
            <div class="b-form-app__title">The application for this object</div>
            <div class="e-form-value__name"><input placeholder="Name, surname" ></div>
            <div class="e-form-value__phone"><input placeholder="Contact phone"></div>
            <div class="e-form-value__country"><input placeholder="Yout country ot residence"></div>
            <div class="e-form-value__email"><input placeholder="Email"></div>
            <div class="e-form-comments"><textarea placeholder="Your comments" rows="4"></textarea></div>
            <div class="b-form-captcha">
                <img class="e-form-captcha" alt="" src="/storage/captcha.png">
                <input placeholder="Enter code shown">
            </div>
            <div class="e-form-button"><input type="submit" value="SEND"></div>
        </form>
    </div>
    <div class="b-map__sale"></div>
    <div class="l-calc">
        <div class="e-title__calc">Ипотечный калькулятор</div>
        <div class="b-form-calc__calc">
            <div class="b-category__form-calc">
                <input class="e-radio__cat-calc" type="radio" value="count" name="cat-calc" id="count"><label class="e-count__category" for="count">По стоимости недвижимости</label>
                <input class="e-radio__cat-calc" type="radio" value="sum" name="cat-calc" id="sum"><label class="e-sum__category" for="sum">По сумме кредита</label>
            </div>
            <div class="e-count__form-calc" >
                <input class="e-input__count " placeholder="Стоимость недвижимости">
                <div class="b-cur__count">
                    <select class="e-cur__count">
                        <option>$</option>
                        <option>€</option>
                    </select>
                </div>
            </div>
            <input class="e-rate-form-calc" placeholder="Процентная ставка, %">
            <div class="e-fee__form-calc" >
                <input class="e-input__fee " placeholder="Первоначальный взнос">
                <div class="b-cur__fee">
                    <select class="e-cur__fee">
                        <option>$</option>
                        <option>€</option>
                    </select>
                </div>
            </div>
            <input class="e-time__form-calc" placeholder="Срок кредита, месяцев">
            <div class="b-payments__form-calc" >
                    <span class="e-payment__payments" >
                        <input class="e-radio__payments" type="radio" name="payment" id="annuity" value="annuity">
                        <label  class="e-label__payment" for="annuity">Аннуитетный</label>
                    </span>
                    <span class="e-payment__payments" >
                        <input class="e-radio__payments" type="radio" name="payment"  id="diff" value="diff">
                        <label  class="e-label__payment" for="diff">Дифференцированный</label>
                    </span>
            </div>
            <div class="e-monthly__form-calc" >
                <input class="e-input__monthly " placeholder="Ежемесячные комиссии">
                <div class="b-cur__mountly">
                    <select class="e-cur__monthly">
                        <option>% от суммы кредита</option>
                        <option>% от суммы кредита</option>
                    </select>
                </div>
            </div>
            <div class="b-start__form-calc">
                <span class="e-text__start">Дата начала выплат</span>
                <input class="e-date__start" placeholder="24.06.2015" name="calender">
            </div>
            <div class="e-at__form-calc" >
                <input class="e-input__at " placeholder="Единовременная комиссия">
                <div class="b-cur__at">
                    <select class="e-cur__at">
                        <option>% от суммы кредита</option>
                        <option>% от суммы кредита</option>
                    </select>
                </div>
            </div>
            <div class="b-total__form-calc">
                <div class="b-stat__total">
                    <div class="b-summ__total">
                        <span class="e-type__summ">Сумма кредита:</span>
                        <span class="e-value__summ"><span class="e-num__value">100.000</span>&nbsp;$</span>
                    </div>
                    <div class="b-summ__total">
                        <span class="e-type__summ">Ежемесячный платеж:</span>
                        <span class="e-value__summ"><span class="e-num__value">10.000</span>&nbsp;$</span>
                    </div>
                    <div class="b-summ__total">
                        <span class="e-type__summ">Все выплаты:</span>
                        <span class="e-value__summ"><span class="e-num__value">100.000</span>&nbsp;$</span>
                    </div>
                    <div class="b-summ__total">
                        <span class="e-type__summ">Переплата по кредиту:</span>
                        <span class="e-value__summ"><span class="e-num__value">10.000</span>&nbsp;$</span>
                    </div>
                </div>
                <div class="b-ending__total">
                    <span class="e-text__ending">Окончание выплат:</span>
                    <span class="e-value__ending">21.06.2050</span>
                </div>
            </div>
        </div>
    </div>
    @include('parts.offers')
    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "Organization",
                  "url" : "http://www.tenerifecenter.com",
                  "name": "VYM Canarias",
                  "legalName": "VYM Canarias",
                  "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Tenerife, Spain",
                    "postalCode": " ",
                    "streetAddress": "Las Americas"
                  },
                  "email": "",
                  "faxNumber": "+34 922 713 395",
                  "telephone":  [
                      "+34 922 787 210",
                      "+34 922 719 553",
                      "+34 922 717 663",
                      "+34 922 713 395"
                  ],
                  "logo": "b-logo.png",
                  "member": [
                    {
                      "@type": "Organization"
                    }
                  ],
                  "employee": [
                      {
                          "@type": "Person",
                          "email": "vym.margarita@gmail.com",
                          "image": "p1.png",
                          "jobTitle": "Director",
                          "name": "Margarita Anikyeva",
                          "telephone": "+34 661 259 125"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.russel@gmail.com",
                          "image": "p2.png",
                          "jobTitle": "Director",
                          "name": "Russel Weightman",
                          "telephone": "+34 992 713 395"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.john@gmail.com",
                          "image": "p3.png",
                          "jobTitle": "Sales Manager",
                          "name": "John Doe",
                          "telephone": "+34 922 713 595"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.mary@gmail.com",
                          "image": "p4.png",
                          "jobTitle": "Sales Manager",
                          "name": "Maria Smith",
                          "telephone": "+34 922 713 695"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.megan@gmail.com",
                          "image": "p5.png",
                          "jobTitle": "Office Manager",
                          "name": "Megan Fox",
                          "telephone": "+34 922 713 772"
                      },
                         {
                          "@type": "Person",
                          "email": "vym.emma@gmail.com",
                          "image": "p6.png",
                          "jobTitle": "Office Manager",
                          "name": "Emma Watson",
                          "telephone": "+34 922 713 872"
                      },
                                            {
                          "@type": "Person",
                          "email": "vym.rose@gmail.com",
                          "image": "p7.png",
                          "jobTitle": "Office Manager",
                          "name": "Rose Byrne",
                          "telephone": "+34 661 720 872"
                      }
                  ]
          }
    </script>
@stop

@section('javascript')
    <script type="text/javascript" src="/includes/sale-map.js"></script>
    <script type="text/javascript"  src="/includes/scripts.js"></script>
    <script>
        Select.init({
            selector: '.js-e-choose-value',
            className: 'select-theme-default'
        });
        Select.init({
            selector: '.e-search-action',
            className: 'select-theme-action'
        });
        Select.init({
            selector: '.e-time__search',
            className: 'select-theme-time'
        });
        Select.init({
            selector: '.e-cur__fee',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__count',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__monthly',
            className: 'select-theme-comis'
        });
        Select.init({
            selector: '.e-cur__at',
            className: 'select-theme-comis'
        });
    </script>
@stop