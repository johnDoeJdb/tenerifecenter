<script>
    // "git@github.com:googlemaps/js-marker-clusterer.git#gh-pages"
    angular
        .module('Search', ['ngRoute', 'ngCookies', 'ngSanitize']) // , 'sf.virtualScroll'
        .config(['$locationProvider', function($locationProvider) {
            $locationProvider.html5Mode(true);
        }])
        .controller('SearchController', ['$scope', '$location', '$route', '$http', '$timeout', '$cookieStore', function($scope, $location, $route, $http, $timeout, $cookieStore) {
            $scope.locale = null;
            $scope.countFavorites = 0;
            $scope.countFound = 0;
            $scope.listType = 'search'; // or favorites
            $scope.objects = [];
            $scope.objectsLoading = true;
            $scope.object;
            $scope.mapObject;
            $scope.mode = 'map';
            $scope.page;
            $scope.options = {
                types: [],
                tags: [],
                prices_from: [],
                prices_to: []
            };
            $scope.tags = [];
            $scope.search = angular.copy($location.search());

            $scope.search.for = $scope.search.type_deal || 'sale';
            $scope.search.tag = $scope.search.tag || [];
            $scope.search.price_from = $scope.search.price_from ? parseInt($scope.search.price_from) : null;
            $scope.search.price_to = $scope.search.price_to ? parseInt($scope.search.price_to) : null;
            $scope.search.place = $scope.search.place ? parseInt($scope.search.place) : null;
            $scope.search.bedrooms = $scope.search.bedrooms ? parseInt($scope.search.bedrooms) : null;
            $scope.search.complex = $scope.search.complex ? parseInt($scope.search.complex) : null;
            $scope.search.type = $scope.search.type ? parseInt($scope.search.type) : null;

            var foundObjects = [],
                favorites = $cookieStore.get('favorites') || [],
                favoritedObjects = [],
                map,
                infoWindow,
                mapReloadTimeout,
                markers = [],
            // cluster,
                filterBounds;

            var getPrices = function(side) {
                var from = side == 'from';
                var result = [];
                var numbers = {
                    sale: {
                        min: 10000,
                        max: 20000000,
                        steps: [
                            [100000, 5000],
                            [300000, 20000],
                            [600000, 50000],
                            [1000000, 100000],
                            [null, 500000]
                        ]
                    },
                    rent: {
                        min: 250,
                        max: 5000,
                        steps: [
                            [1000, 50],
                            [1500, 100],
                            [2000, 250],
                            [3000, 500],
                            [null, 1000]
                        ]
                    }
                };
                for (var i = numbers[$scope.search.for].min; i < numbers[$scope.search.for].max;) {
                    if (i < numbers[$scope.search.for].steps[0][0]) {
                        i += numbers[$scope.search.for].steps[0][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[1][0]) {
                        i += numbers[$scope.search.for].steps[1][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[2][0]) {
                        i += numbers[$scope.search.for].steps[2][1];
                    }
                    else if (i < numbers[$scope.search.for].steps[3][0]) {
                        i += numbers[$scope.search.for].steps[3][1];
                    }
                    else {
                        i += numbers[$scope.search.for].steps[4][1];
                    }
                    if (from && $scope.search.price_to && $scope.search.price_to <= i) {
                        break;
                    }
                    else if (!from && $scope.search.price_from && $scope.search.price_from >= i) {
                        continue;
                    }
                    result.push(i);
                }
                return result;
            };

            $scope.formatPrice = function(price) {
                return '€ ' + price.format(0, 3, ' ');
            };

            $scope.countFavorites = favorites.length;

            var request = function(data, cb) {
                $http.post('/' + $scope.locale + '/api/search', data).success(cb);
            };

            var getTags = function(id, exclude) {
                var arr = true;
                if (typeof id != 'array') {
                    id = [id];
                    arr = false;
                }
                var result = [];
                console.log($scope.tags, id, exclude);
                angular.forEach($scope.tags, function(tag) {
                    if (id.indexOf(tag.id) != -1) {
                        if (!exclude) {
                            result.push(tag);
                        }
                    }
                    else if (exclude) {
                        result.push(tag);
                    }
                });
                console.log(arr, !arr && !exclude ? result[0] : result);
                return !arr && !exclude ? result[0] : result;
            };

            $scope.run = function() {
                $scope.options.types = getTags($scope.tagId.type).options;
                $scope.options.tags = getTags($scope.tagId.type, true);

                $scope.listType = 'search';
                $scope.objectsLoading = true;
                $scope.objects = [];
                $scope.updatePrices();
                request($scope.search, function(response) {
                    foundObjects = response.data;
                    $scope.countFound = foundObjects.length;
                    updateData(foundObjects);
                });
            };

            $scope.updatePrices = function() {
                $scope.options.prices_from = getPrices('from');
                $scope.options.prices_to = getPrices('to');
            };

            $scope.clickSearchTab = function() {
                $scope.listType = 'search';
                $scope.objectsLoading = true;
                $scope.objects = [];
                $scope.updatePrices();
                $scope.search.price_from = null;
                $scope.search.price_to = null;
                request($scope.search, function(response) {
                    foundObjects = response.data;
                    $scope.countFound = foundObjects.length;
                    updateData(foundObjects);
                });
            };

            $scope.clickFavoritesTab = function() {
                $scope.listType = 'favorites';
                $scope.objectsLoading = true;
                $scope.objects = [];
                request({favorites: favorites}, function(response) {
                    favoritedObjects = response.data;
                    updateData(favoritedObjects);
                });
            };

            $scope.searchRequest = function() {
                $scope.listType = 'search';
                $scope.objectsLoading = true;
                $scope.objects = [];

                var tags = [];
                angular.forEach($scope.options.tags, function(tag) {
                    if (tag.value) {
                        tags.push(parseInt(tag.value));
                    }
                    else if (tag.checked) {
                        tags.push(tag.id);
                    }
                });
                $scope.search.tag = tags;

                request($scope.search, function(response) {
                    foundObjects = response.data;
                    $scope.countFound = foundObjects.length;
                    updateData(foundObjects);
                });
            };

            $scope.toggleTag = function(tagId) {
                var idx = $scope.search.tag.indexOf(tagId);
                if (idx > -1) {
                    $scope.search.tag.splice(idx, 1);
                }
                else {
                    $scope.search.tag.push(tagId);
                }
                $scope.searchRequest();
            };

            var updateData = function(objects) {

                $scope.objects = [];

                //cluster.clearMarkers();
                markers.forEach(function(m) {
                    m.setMap(null);
                });
                markers = [];

                if (!objects.length) {
                    $scope.objectsLoading = false;
                }

                $timeout(function() {
                    var bounds = new google.maps.LatLngBounds();
                    for (i in objects) {
                        objects[i].favorited = favorites.indexOf(objects[i].id) != -1;
                        if (objects[i].lat && objects[i].lng) {
                            var marker = new google.maps.Marker({
                                map: map,
                                icon: '/assets/images/dot.png',
                                position: new google.maps.LatLng(objects[i].lat, objects[i].lng),
                                object: objects[i]
                            });
                            objects[i].marker = marker;
                            bounds.extend(marker.getPosition());

                            google.maps.event.addListener(marker, 'click', (function(marker) {
                                return function(event) {
                                    $scope.object = marker.object;
                                    $scope.mode = 'object';
                                    $scope.page = '/' + $scope.locale + '/api/object/' + $scope.object.id;
                                    $scope.$apply();
                                }
                            })(marker));

                            google.maps.event.addListener(marker, 'mouseover', (function(marker) {
                                return function() {
                                    $scope.mapObject = marker.object;
                                    //infoWindow.setContent(document.getElementById('infoWindow'));
                                    infoWindow.open(map, marker);
                                    $scope.$apply();
                                }
                            })(marker));

                            markers.push(marker);
                        }
                        $scope.objects.push(objects[i]);
                    }

                    if (markers.length) {
                        map.fitBounds(bounds);
                        // cluster.addMarkers(markers);
                        // cluster.fitMapToMarkers();
                    }

                    $scope.objectsLoading = false;
                }, 1);

            };

            var mapOptions = {
                disableDefaultUI: true,
                zoomControl: true,
                scaleControl: true,
                minZoom: 9,
                maxZoom: 17,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoom: 13,
                center: new google.maps.LatLng(28.0629, -16.7239)
            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            // cluster = new MarkerClusterer(map, []);
            infoWindow = new google.maps.InfoWindow({
                maxWidth: 300,
                disableAutoPan: true,
                content: document.getElementById('infoWindow')
            });

            google.maps.event.addListenerOnce(infoWindow, 'closeclick', function() {
                $scope.$apply();
            });

            google.maps.event.addListenerOnce(map, 'idle', function() {
                google.maps.event.addListener(map, 'bounds_changed', function() {
                    if (mapReloadTimeout) {
                        $timeout.cancel(mapReloadTimeout);
                    }
                    mapReloadTimeout = $timeout(function() {
                        filterBounds = map.getBounds();
                        $scope.objects = [];
                        ($scope.listType == 'search' ? foundObjects : favoritedObjects).forEach(function(o) {
                            if (o.lat && o.lng && !filterBounds.contains(o.marker.position)) {
                                return;
                            }
                            $scope.objects.push(o);
                        });
                    }, 300);
                });
            });


            $scope.clickObject = function(object) {
                var typeDeal = $scope.search.for;
                $scope.object = object;
                window.open('/' + typeDeal + '/' + $scope.object.id) ;
            };

            $scope.clickBackToSearch = function() {
                $scope.mode = 'map';
            };

            $scope.clickFavorite = function(object, $event) {
                $event.stopPropagation();
                var i = favorites.indexOf(object.id);
                if (i === -1) {
                    object.favorited = true;
                    favorites.push(object.id);
                }
                else {
                    favorites.splice(i, 1);
                    object.favorited = false;
                }
                $cookieStore.put('favorites', favorites);
                $scope.countFavorites = favorites.length;
            };

            $scope.mouseOver = function(object) {
                if (!object.lat) {
                    infoWindow.close();
                    return;
                }
                $scope.mapObject = object;
                //infoWindow.setContent(document.getElementById('infoWindow'));
                //infoWindow.setPosition(object.marker.getPosition());
                infoWindow.open(map, object.marker);
            };

        }])
</script>



