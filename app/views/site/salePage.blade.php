@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div ng-init="
        objects = '{{ base64_encode(json_encode($objects, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK)) }}';
        objects = $root.unpackValue(objects);
        "
    >
    <div class="l-category">
        <div class="b-category__category">
            <a class="e-cat__category" href="">Общая база</a>
            <a class="e-cat__category" href="">Новострой</a>
            <a class="e-cat__category" href="">Вторичное жилье</a>
            <a class="e-cat__category" href="">Элитное</a>
            <a class="e-cat__category" href="">Спецпредложения</a>
            <a class="e-cat__category" href="">Банковская</a>
            <a class="e-cat__category" href="">Инвестиции</a>
            <a class="e-cat__category" href="">Снижена цена</a>
            <a class="e-cat__category" href="">Последние поступления</a>
        </div>
    </div>
    <div class="l-search">
        @include('parts.search-form')
    </div>
    @include('parts.offers')
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url" : "http://www.tenerifecenter.com",
            "name": "VYM Canarias",
            "legalName": "VYM Canarias",
            "address": {
            "@type": "PostalAddress",
            "addressLocality": "Tenerife, Spain",
            "postalCode": " ",
            "streetAddress": "Las Americas"
            },
            "email": "",
            "faxNumber": "+34 922 713 395",
            "telephone":  [
              "+34 922 787 210",
              "+34 922 719 553",
              "+34 922 717 663",
              "+34 922 713 395"
            ],
            "logo": "b-logo.png",
            "member": [
            {
              "@type": "Organization"
            }
            ],
            "employee": [
              {
                  "@type": "Person",
                  "email": "vym.margarita@gmail.com",
                  "image": "p1.png",
                  "jobTitle": "Director",
                  "name": "Margarita Anikyeva",
                  "telephone": "+34 661 259 125"
              },
              {
                  "@type": "Person",
                  "email": "vym.russel@gmail.com",
                  "image": "p2.png",
                  "jobTitle": "Director",
                  "name": "Russel Weightman",
                  "telephone": "+34 992 713 395"
              },
              {
                  "@type": "Person",
                  "email": "vym.john@gmail.com",
                  "image": "p3.png",
                  "jobTitle": "Sales Manager",
                  "name": "John Doe",
                  "telephone": "+34 922 713 595"
              },
              {
                  "@type": "Person",
                  "email": "vym.mary@gmail.com",
                  "image": "p4.png",
                  "jobTitle": "Sales Manager",
                  "name": "Maria Smith",
                  "telephone": "+34 922 713 695"
              },
              {
                  "@type": "Person",
                  "email": "vym.megan@gmail.com",
                  "image": "p5.png",
                  "jobTitle": "Office Manager",
                  "name": "Megan Fox",
                  "telephone": "+34 922 713 772"
              },
                 {
                  "@type": "Person",
                  "email": "vym.emma@gmail.com",
                  "image": "p6.png",
                  "jobTitle": "Office Manager",
                  "name": "Emma Watson",
                  "telephone": "+34 922 713 872"
              },
                                    {
                  "@type": "Person",
                  "email": "vym.rose@gmail.com",
                  "image": "p7.png",
                  "jobTitle": "Office Manager",
                  "name": "Rose Byrne",
                  "telephone": "+34 661 720 872"
              }
            ]
        }
    </script>
@stop

@section('javascript')
    <script type="text/javascript" src="/includes/sale-map.js"></script>
    <script type="text/javascript"  src="/includes/scripts.js"></script>
    <script>
        Select.init({
            selector: '.js-e-choose-value',
            className: 'select-theme-default'
        });
        Select.init({
            selector: '.e-search-action',
            className: 'select-theme-action'
        });
        Select.init({
            selector: '.e-time__search',
            className: 'select-theme-time'
        });
        Select.init({
            selector: '.e-cur__fee',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__count',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__monthly',
            className: 'select-theme-comis'
        });
        Select.init({
            selector: '.e-cur__at',
            className: 'select-theme-comis'
        });
    </script>
@stop