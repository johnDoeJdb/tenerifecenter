@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div ng-init="
        objects = '{{ base64_encode(json_encode($objects, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK)) }}';
        objects = $root.unpackValue(objects);
        "
    >
    <div class="l-category">
        <div class="b-category__category">
            <a class="e-cat__category" href="">Общая база</a>
            <a class="e-cat__category" href="">Новострой</a>
            <a class="e-cat__category" href="">Вторичное жилье</a>
            <a class="e-cat__category" href="">Элитное</a>
            <a class="e-cat__category" href="">Спецпредложения</a>
            <a class="e-cat__category" href="">Банковская</a>
            <a class="e-cat__category" href="">Инвестиции</a>
            <a class="e-cat__category" href="">Снижена цена</a>
            <a class="e-cat__category" href="">Последние поступления</a>
        </div>
    </div>
    <div class="l-search">
        @include('parts.search-form')
    </div>
    <div class="l-form">
        <div class="b-form-info">
            <div class="b-form-info__col1">
                <img class="e-form-photo" src="/storage/Object photo.png" alt="">
                <div class="b-form-photos">
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel">
                            <ul>
                                <li><img src="/storage/Object photo.png" alt="Image 1" onclick="changeImage('storage/Object photo.png')"></li>
                                <li><img src="/storage/photo1.png" alt="Image 2" onclick="changeImage('storage/photo1.png')"></li>
                                <li><img src="/storage/photo2.png" alt="Image 3" onclick="changeImage('storage/photo2.png')"></li>
                                <li><img src="/storage/photo3.png" alt="Image 4" onclick="changeImage('storage/photo3.png')"></li>
                                <li><img src="/storage/photo4.png" alt="Image 5" onclick="changeImage('storage/photo4.png')" ></li>
                                <li><img src="/storage/photo1.png" alt="Image 6" onclick="changeImage('storage/photo1.png')"></li>
                            </ul>
                        </div>
                        <a href="#" class="jcarousel-control-prev"></a>
                        <a href="#" class="jcarousel-control-next"></a>
                    </div>
                </div>
                <div class="b-form-count">
                    <span class="e-form-code">Code: VR1507</span>
                    <span class="e-form-count">{{number_format($object['price'], 0, ',', ' ')}} € / month</span>
                </div>
                <div class="e-form-desc">Large Studio apartment in the complex Las Barandos on the first line of beach in Callao Salvaje. The complex is located right near of ocean and you can enjoy great surf right from the terrace of his apartment. The apartment consists of living room, large bathroom and open plan kitchen. There is also an outdoor terrace with amazing sea (website hidden) Callao Salvaje is a new beach a few minutes walk from Las Barandas.  The complex has a swimming pool.</div>
            </div>
            <div class="b-form-info__col2">
                <div class="b-form-subj">{{ $object['name-ru'] }}</div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Property type:</div>
                    <div class="e-form-parm__value">{{ $propertyType['name-ru'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Location:</div>
                    <div class="e-form-parm__value">{{ $place['name-ru'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Living space:</div>
                    <div class="e-form-parm__value">{{ $object['living_space'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Total area:</div>
                    <div class="e-form-parm__value">{{ $object['gross_area'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Number of bedrooms:</div>
                    <div class="e-form-parm__value">{{ $object['number_bedrooms'] }}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Pool:</div>
                    <div class="e-form-parm__value">{{$pool['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Garage/Parking:</div>
                    <div class="e-form-parm__value">{{$garage['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">View:</div>
                    <div class="e-form-parm__value">{{$view['name-ru']}}</div>
                </div>
                <div class="b-form-parm">
                    <div class="e-form-parm__name">Furniture:</div>
                    <div class="e-form-parm__value">{{$furniture['name-ru']}}</div>
                </div>
                <div class="b-form-contacts">
                    <div class="e-form-contacts__title">Manager contacts:</div>
                    <div class="b-skype__contact">
                        <img class="e-icon__skype" alt="" src="/includes/e-icon__skype.png">
                        <div class="e-login__skype">manager_skype</div>
                    </div>
                    <div class="b-phone__contact">
                        <img class="e-icon__phone" alt="" src="/includes/e-icon__phone.png">
                        <div class="e-form-contact__info">+34 922 719 553,<br>+342 992 717 663,<br>+34 922 713 395</div>
                    </div>
                </div>
                <div class="b-pay__rent">
                    <img class="e-visa__pay" alt="" src="/includes/e-visa.png">
                    <img class="e-mcart__pay" alt="" src="/includes/e-mcart.png">
                    <img class="e-amex__pay" alt="" src="/includes/e-amex.png">
                    <img class="e-discover__pay" alt="" src="/includes/e-discover.png">
                    <img class="e-paypal__pay" alt="" src="/includes/e-paypal.png">
                </div>
            </div>
            <div class="b-button__rent">
                <a class="e-request__button" href="">Request to Book</a>
                <a class="e-checkout__button" href="">Checkout now</a>
            </div>
        </div>
    </div>
    <div class="l-app__rent">
        <form class="b-form__app-rent" name="form" method="post" action="">
            <div class="b-form-app__title">The application for this object</div>
            <div class="e-form-value__name"><input placeholder="Name, surname" ></div>
            <div class="e-form-value__phone"><input placeholder="Contact phone"></div>
            <div class="e-form-value__country"><input placeholder="Yout country ot residence"></div>
            <div class="e-form-value__email"><input placeholder="Email"></div>
            <div class="e-form-comments"><textarea placeholder="Your comments" rows="4"></textarea></div>
            <div class="b-form-captcha">
                <img class="e-form-captcha" alt="" src="/storage/captcha.png">
                <input placeholder="Enter code shown">
            </div>
            <div class="e-form-button"><input type="submit" value="SEND"></div>
        </form>
    </div>
    <div class="b-map__sale"></div>
    @include('parts.offers')
    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "Organization",
                  "url" : "http://www.tenerifecenter.com",
                  "name": "VYM Canarias",
                  "legalName": "VYM Canarias",
                  "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Tenerife, Spain",
                    "postalCode": " ",
                    "streetAddress": "Las Americas"
                  },
                  "email": "",
                  "faxNumber": "+34 922 713 395",
                  "telephone":  [
                      "+34 922 787 210",
                      "+34 922 719 553",
                      "+34 922 717 663",
                      "+34 922 713 395"
                  ],
                  "logo": "b-logo.png",
                  "member": [
                    {
                      "@type": "Organization"
                    }
                  ],
                  "employee": [
                      {
                          "@type": "Person",
                          "email": "vym.margarita@gmail.com",
                          "image": "p1.png",
                          "jobTitle": "Director",
                          "name": "Margarita Anikyeva",
                          "telephone": "+34 661 259 125"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.russel@gmail.com",
                          "image": "p2.png",
                          "jobTitle": "Director",
                          "name": "Russel Weightman",
                          "telephone": "+34 992 713 395"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.john@gmail.com",
                          "image": "p3.png",
                          "jobTitle": "Sales Manager",
                          "name": "John Doe",
                          "telephone": "+34 922 713 595"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.mary@gmail.com",
                          "image": "p4.png",
                          "jobTitle": "Sales Manager",
                          "name": "Maria Smith",
                          "telephone": "+34 922 713 695"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.megan@gmail.com",
                          "image": "p5.png",
                          "jobTitle": "Office Manager",
                          "name": "Megan Fox",
                          "telephone": "+34 922 713 772"
                      },
                         {
                          "@type": "Person",
                          "email": "vym.emma@gmail.com",
                          "image": "p6.png",
                          "jobTitle": "Office Manager",
                          "name": "Emma Watson",
                          "telephone": "+34 922 713 872"
                      },
                                            {
                          "@type": "Person",
                          "email": "vym.rose@gmail.com",
                          "image": "p7.png",
                          "jobTitle": "Office Manager",
                          "name": "Rose Byrne",
                          "telephone": "+34 661 720 872"
                      }
                  ]
          }
    </script>
@stop

@section('javascript')
    <script type="text/javascript" src="/includes/sale-map.js"></script>
    <script type="text/javascript"  src="/includes/scripts.js"></script>
    <script>
        Select.init({
            selector: '.js-e-choose-value',
            className: 'select-theme-default'
        });
        Select.init({
            selector: '.e-search-action',
            className: 'select-theme-action'
        });
        Select.init({
            selector: '.e-time__search',
            className: 'select-theme-time'
        });
        Select.init({
            selector: '.e-cur__fee',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__count',
            className: 'select-theme-cur'
        });
        Select.init({
            selector: '.e-cur__monthly',
            className: 'select-theme-comis'
        });
        Select.init({
            selector: '.e-cur__at',
            className: 'select-theme-comis'
        });
    </script>
@stop