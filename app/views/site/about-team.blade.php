@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div class="l-about">
        <div class="l-inner__about">
            @include('parts.about-menu')
            <span class="e-aboutus-title">About Us</span>
            <div class="b-team__about">
                <div class="e-head__team"><span class="e-company__team">"VYM CANARIAS"</span> - real estate company (Tenerife, Islas Canarias).</div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.  Aenean massa. Cum sociis natoque
                    penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                    Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis  pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                <p>Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                <p>Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean
                    imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget
                    condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
                    hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus.</p>
                <p>Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis
                    magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc</p>
            </div>
            <div class="b-employers__team">
                <div class="b-row__employers">
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p1.png" alt="">
                            <div class="e-company__empl">VYM Canarias Victoria</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Margarita Anikyeva</div>
                                <div class="e-position__person">Director</div>
                                <div class="e-mail__person">vym.margarita@gmail.com</div>
                                <div class="e-phone__person">+34 661 259 125</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh</p>
                        </div>
                    </div>
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p2.png" alt="">
                            <div class="e-company__empl">VYM Canarias Victoria</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Russel Weightman</div>
                                <div class="e-position__person">Director</div>
                                <div class="e-mail__person">vym.russel@gmail.com</div>
                                <div class="e-phone__person">+34 992 713 395</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                </div>
                <div class="b-row__employers">
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p3.png" alt="">
                            <div class="e-company__empl">VYM Canarias, El Beril, La Caleta</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">John Doe</div>
                                <div class="e-position__person">Sales Manager</div>
                                <div class="e-mail__person">vym.john@gmail.com</div>
                                <div class="e-phone__person">+34 922 713 595</div>
                            </div>
                            <a class="b-contact__empl"  href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p4.png" alt="">
                            <div class="e-company__empl">VYM Canarias, El Beril, La Caleta</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Maria Smith</div>
                                <div class="e-position__person">Sales Manager</div>
                                <div class="e-mail__person">vym.mary@gmail.com</div>
                                <div class="e-phone__person">+34 922 713 695</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                </div>
                <div class="b-row__employers">
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p5.png" alt="">
                            <div class="e-company__empl">VYM Canarias, Playa Paraiso</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Megan Fox</div>
                                <div class="e-position__person">Office Manager</div>
                                <div class="e-mail__person">vym.megan@gmail.com</div>
                                <div class="e-phone__person">+34 922 713 772</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p6.png" alt="">
                            <div class="e-company__empl">VYM Canarias, Playa Paraiso</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Emma Watson</div>
                                <div class="e-position__person">Office Manager</div>
                                <div class="e-mail__person">vym.emma@gmail.com</div>
                                <div class="e-phone__person">+34 922 713 872</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                </div>
                <div class="b-row__employers">
                    <div class="b-empl__employers">
                        <div class="b-row__empl">
                            <img class="e-photo__empl" src="/storage/p7.png" alt="">
                            <div class="e-company__empl">VYM Canarias, Playa Paraiso</div>
                            <div class="b-person__empl">
                                <div  class="e-name__person">Rose Byrne</div>
                                <div class="e-position__person">Office Manager</div>
                                <div class="e-mail__person">vym.rose@gmail.com</div>
                                <div class="e-phone__person">+34 922 720 872</div>
                            </div>
                            <a class="b-contact__empl" href="">
                                <img class="e-mail__empl" src="/includes/e-mail__empl.png" alt="">
                                <span class="e-contact__empl">Contact this person</span>
                            </a>
                        </div>
                        <div class="b-descr__empl">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed erat
                                tellus, auctor non dignissim vulputate, luctus quis sem. Proin arcu
                                libero, posuere a dignissim in, porta eu nibh.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="/includes/scripts.js"></script>
@stop