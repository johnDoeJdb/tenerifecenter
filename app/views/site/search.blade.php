<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="stylesheet" href="/assets/stylesheets/site_old.css">
        <script src="//maps.googleapis.com/maps/api/js?libraries=places&sensor=false&region=es&language=es"></script>
        <script src="/assets/javascript/site_old.js"></script>

        <base href="/{{ App::getLocale() }}/" />

        <title>Tenerifecenter</title>

        <style>
            html, body {width:100%;height: 100%;}

            #map {
                position: absolute;
                width: 100%;
                height: 100%;
            }

            .column {overflow-x: hidden; overflow-y: scroll; height: 100%;}

            .header-line {position: absolute; width: 100%;}

            .object { cursor: pointer; margin: 15px 0; padding: 5px; border: 1px solid gray; border-radius: 5px; }
            .object.active { box-shadow: 2px 2px 10px gray; }
            .modeObject .object.active { margin-right: -16px; border-top-right-radius: 0; border-bottom-right-radius: 0; border-right: 0; z-index: 2; }

            .nav>li:first-child { margin-left: 15px; }
            .nav>li>a { padding: 8px 15px; }

        </style>
    </head>

    <body>
        @include('parts.header_line')

        <div class="container-fluid ng-cloak"
             style="position: absolute; height: 100%; width: 100%; padding: 41px 0 0;"
             ng-app="Search"
             ng-controller="SearchController"
             ng-init="
                tags = {{{ json_encode($tags, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK) }}};
                tagId = {{{ json_encode($tagId, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK) }}};
                places = {{{ json_encode($places, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK) }}};
                complexes = {{{ json_encode($complexes, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK) }}};
                locale = '{{ App::getLocale() }}';
                run();
                ">

            <div class="row" style="height: 100%; margin: 0; border-top: 1px solid gray;">
                <div class="col-sm-2 column" ng-hide="mode == 'object'" style="border-right: 1px solid gray;">

                    <ul class="nav nav-tabs" style="margin: 5px -15px;">
                        <li role="presentation" ng-class="{active: search.for == 'rent'}" ng-click="search.for = 'rent'; clickSearchTab()"><a href="">Аренда</a></li>
                        <li role="presentation" ng-class="{active: search.for == 'sale'}" ng-click="search.for = 'sale'; clickSearchTab()"><a href="">Продажа</a></li>
                    </ul>

                    <form class="form-horizontal" role="form">

                        <div class="row" style="padding-bottom: 5px;">
                            <div class="col-sm-12">
                                Цена
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <select name="price_from" class="form-control" ng-model="search.price_from" ng-options="price as formatPrice(price) for price in options.prices_from track by price" ng-change="updatePrices(); searchRequest()">
                                    <option value="">{{ trans('site.search.price_from') }}</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="price_to" class="form-control" ng-model="search.price_to" ng-options="price as formatPrice(price) for price in options.prices_to track by price" ng-change="updatePrices(); searchRequest()">
                                    <option value="">{{ trans('site.search.price_to') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <select name="place" class="form-control" ng-model="search.place" ng-options="place.id as place.name group by place.region for place in places" ng-change="searchRequest()">
                                    <option value="">{{ trans('site.search.place') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <select name="place" class="form-control" ng-model="search.complex" ng-options="complex.id as complex.name for complex in complexes" ng-change="searchRequest()">
                                    <option value="">{{ trans('site.search.complex') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <select name="type" class="form-control" ng-model="search.type" ng-options="type.id as type.name for type in options.types" ng-change="searchRequest()">
                                    <option value="">{{ trans('site.search.type') }}</option>
                                </select>
                            </div>
                        </div>

                        <div ng-repeat="tag in options.tags | filter: {group: 1}" class="form-group">
                            <label class="col-sm-6 control-label">@{{ tag.name }}</label>
                            <div class="col-sm-6">
                                <select class="form-control" ng-model="tag.value" ng-change="searchRequest()">
                                    <option value="">Не указано</option>
                                    <option ng-value="option.id" ng-repeat="option in tag.options" ng-bind="option.name" ng-selected="tag.value == option.id"></option>
                                </select>
                            </div>
                        </div>

                        <div ng-repeat="tag in options.tags | filter: {group: 0}" class="form-group">
                            <div class="col-sm-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="tag.checked" ng-change="searchRequest()">
                                        @{{ tag.name }}
                                    </label>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>

                <div class="col-sm-4" style="border-right: 1px solid gray; height: 100%; padding: 0;" ng-class="{'modeObject': mode == 'object'}">

                    <div style="position: absolute; width: 100%; z-index: 1; left: 0; top: 5px;">
                        <ul class="nav nav-tabs">
                            <li role="presentation" ng-class="{active: listType == 'search'}" ng-click="clickSearchTab()"><a href="">Найденные <span class="badge" ng-bind="countFound" ng-show="countFound"></span></a></li>
                            <li role="presentation" ng-class="{active: listType == 'favorites'}" ng-click="clickFavoritesTab()"><a href="">Избранные <span class="badge" ng-bind="countFavorites" ng-show="countFavorites"></span></a></li>
                        </ul>
                    </div>

                    <div style="position: absolute; width: 100%; height: 100%; padding: 50px 15px 0; overflow-x: hidden; overflow-y: scroll;">

                        <ul style="list-style: none; padding: 0; height: 100%; margin: 0; overflow-x: hidden; overflow-y: scroll;">

                            <li ng-show="objectsLoading" class="text-center" style="padding-top: 70px;">Загрузка...</li>
                            <li ng-show="!objectsLoading && !objects.length" class="text-center" style="padding-top: 70px;">Ничего не найдено</li>

                            <li ng-repeat="o in objects" ng-click=":: clickObject(o)" style="height: 100px; border: 1px solid gray; margin-bottom: 5px; padding: 10px; cursor: pointer;" ng-mouseover=":: mouseOver(o)">

                                <div class="image" style="width: 100px; height: 80px; float: left; overflow: hidden;">
                                    <img ng-src="/images/objects/@{{ :: o.images[0].preview }}" style="width: 100%;" ng-if=":: o.images[0]" />
                                </div>
                                <div class="info" style="width: 250px; height: 80px; float: right;">
                                    <span class="glyphicon" style="font-size: 17px; color: darkred;" ng-class="{'glyphicon-heart': o.favorited, 'glyphicon-heart-empty': !o.favorited}" ng-click="clickFavorite(o, $event)"></span>
                                    @{{ o.name }} <b>&euro;@{{ o.price }}</b>
                                    <span ng-bind-html="o.teaser"></span>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>

                <div class="col-sm-6 column" ng-show="mode == 'map'" style="padding: 0;">
                    <div id="map"></div>
                </div>

                <div class="col-sm-8 column" style="padding: 15px;" ng-show="mode == 'object'">
                    <ol class="breadcrumb">
                        <li><a href="" ng-click="clickBackToSearch()">&larr; К результатам поиска</a></li>
                    </ol>
                    <div ng-include="page"></div>
                </div>
            </div>

            <div style="display:none;">
                <div id="infoWindow" style="height: 100px;">
                    <div class="image" style="width: 100px; height: 80px; float: left; overflow: hidden; padding-right: 5px;">
                        <img ng-src="/images/objects/@{{ mapObject.images[0].preview }}" style="width: 100%;" ng-if="mapObject.images[0]" />
                    </div>
                    @{{ mapObject.name }}
                    <span class="glyphicon" style="font-size: 17px; color: darkred;" ng-class="{'glyphicon-heart': mapObject.favorited, 'glyphicon-heart-empty': !mapObject.favorited}" ng-click="clickFavorite(mapObject, $event)"></span>
                    <br />
                    <b>@{{ mapObject.price }} &euro;</b>
                    <span ng-bind-html="mapObject.teaser"></span>
                </div>
            </div>

        </div>

    </body>

    @include('site.search-js')

</html>