@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div class="l-about">
        <div class="l-inner__about">
            @include('parts.about-menu')
            <span class="e-aboutus-title">About Us</span>
            <div class="b-jobs__about">
                <div class="b-prologue__jobs">
                    <div class="e-head__prologue">Looking for a job?</div>
                    <div class="e-content__prologue">Becoming part of the SecondHome Tenerife team is just one click away.</div>
                </div>
                <div class="b-job__about">
                    <span class="e-vacancy__job">US Sales Representative</span>
                    <div class="b-descr__job">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.  Aenean massa. Cum sociis natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                            Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                            venenatis vitae, justo.</p>
                        <b>Open Territories:</b>
                        <ul>
                            <li>Currently accepting resumes in all territories nationwide</li>
                        </ul>
                        <b>Responsibilities:</b>
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                            <li>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aliquam lorem ante,
                                dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.</li>
                        </ul>
                        <b>Compensation and Benefits:</b>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.  Aenean massa. Cum sociis natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                            Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                            venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.
                            Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean
                            imperdiet.</p>
                        <b>Experience:</b>
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula</li>
                            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                            <li>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</li>
                        </ul>
                    </div>
                    <div class="b-contact__job">
                        <div class="b-about-contact__contact">
                            <div class="e-empl-name__about-contact">Tim Baldwin - HR Manager</div>
                            <div class="e-empl-email__about-contact">vym.hr@www.tenerifecenter.com</div>
                        </div>
                        <a class="e-button__contact" href="">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="/includes/scripts.js"></script>
@stop