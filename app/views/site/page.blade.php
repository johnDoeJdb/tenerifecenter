@extends('layouts.site')

@section('content')
<style>
    img {margin: 15px;}
</style>
<div class="container">

    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-3">
            <ul class="list-group">
                @foreach ($menu as $item)
                    <li class="list-group-item">
                        <a href="/{{ $url_prefix }}/{{ Page::getLocal($item, 'path') }}">{{ Page::getLocal($item, 'name') }}</a>
                        @if ($item->children)

                            <ul>
                                @foreach ($item->children as $item2)
                                    <li>
                                        <a href="/{{ $url_prefix }}/{{ Page::getLocal($item, 'path') }}/{{ Page::getLocal($item2, 'path') }}">{{ Page::getLocal($item2, 'name') }}</a>
                                        @if ($item->children)

                                            <ul>
                                                @foreach ($item2->children as $item3)
                                                    <li>
                                                        <a href="/{{ $url_prefix }}/{{ Page::getLocal($item, 'path') }}/{{ Page::getLocal($item2, 'path') }}/{{ Page::getLocal($item3, 'path') }}">{{ Page::getLocal($item3, 'name') }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>

                                        @endif
                                    </li>
                                @endforeach
                            </ul>

                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-9">

            @if (count($breadcrumbs) > 1)
                <ol class="breadcrumb">
                    @foreach ($breadcrumbs as $item)
                        <li><a href="/{{ $item[0] }}">{{ $item[1] }}</a></li>
                    @endforeach
                </ol>
            @endif

            <h1>{{ Page::getLocal($page, 'name') }}</h1>

            {{ Page::getLocal($page, 'content') }}
        </div>
    </div>

</div>


@stop