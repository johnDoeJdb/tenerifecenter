@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div ng-init="
        objects = '{{ base64_encode(json_encode($objects, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK)) }}';
        objects = $root.unpackValue(objects);
        "
    >

    <div class="l-map">
        <div class="b-map__index"></div>
        @include('parts.main-search')
    </div>
    <div class="l-guide">
        <div class="b-guides__guide">
            <div class="b-offers__guides">
                <img class="e-icon__offers" src="/includes/b-offers.png" alt="">
                <div class="b-title__offers">{{Lang::get('site.tag.special_offers')}}</div>
                <div class="b-enum__offers">
                    <a class="e-offer__enum" href="">VIP</a>
                    <a class="e-offer__enum" href="">Seafront</a>
                    <a class="e-offer__enum" href="">Urgent sale </a>
                    <a class="e-offer__enum" href="">Unique properties</a>
                </div>
            </div>
            <div class="b-locations__guides">
                <img class="e-icon__locations" src="/includes/b-locations.png" alt="">
                <div class="b-title__locations">{{Lang::get('site.tag.popular_locations')}}</div>
                <div class="b-enum__locations">
                    @foreach ($popularPlaces as $key => $place)
                        <a class="e-location__enum" href="/{{Lang::getLocale()}}/search?place={{$key}}">{{$place}}</a>
                    @endforeach
                </div>
            </div>
            <div class="b-propery__guides">
                <img class="e-icon__propery" src="/includes/b-propery.png" alt="">
                <div class="b-title__propery">{{Lang::get('site.tag.property_types')}}</div>
                <div class="b-enum__propery">
                    @foreach ($popularPropertyTypes as $key => $property)
                        <a class="e-location__enum" href="/{{Lang::getLocale()}}/search?type={{$key}}">{{$property}}</a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="b-addplace__guide">{{Lang::get('site.add_place.text')}}<a class="e-button__addplace" href="">{{Lang::get('site.add_place.button')}}</a></div>
    </div>
    <div class="l-content">
        <div class="e-content">
            <h1>
                {{Lang::get('site.welcome.welcome_title')}}
            </h1>
            <p>
                {{Lang::get('site.welcome.text_line1')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line2')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line3')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line4')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line5')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line6')}}
            </p>
            <p>
                {{Lang::get('site.welcome.text_line7')}}
            </p>
        </div>
        <div class="b-altlogo">
            <img class="e-altlogo__altlogo" src="/includes/b-altlogo.png" alt="">
            <div class="b-button__altlogo"><a class="e-button__button" href="">{{Lang::get('site.welcome.we_recommend')}}</a></div>
        </div>
    </div>
    @include('parts.offers')
    <div class="l-news">
        <div class="b-news-head"><span class="e-news-title">{{Lang::get('site.news.latest_news')}}</span></div>
        <div class="b-news-previews">
            <div class="b-news-preview">
                <div class="b-img__preview">
                    <img class="b-news-preview__image" src="/storage/teide-tajinaste-deserto1-580x423.png" alt="">
                    <div class="b-news-preview__descr">Teide is surprising us again unexpectedly in spring!</div>
                </div>
                <div class="b-news-preview__date">26.03.2015</div>
            </div>
            <div class="b-news-preview">
                <div class="b-img__preview">
                    <img class="b-news-preview__image" src="/storage/newhigh87.png" alt="">
                    <div class="b-news-preview__descr">The new highway is about to be opened!</div>
                </div>
                <div class="b-news-preview__date">21.03.2015</div>
            </div>
        </div>
        <div class="b-news-short">
            @foreach ($news as $article)
                <div class="e-news-short-date">{{ date('d.m.Y', $article->published_at) }}</div>
                <div class="e-news-short-text">{{ $article['name-'.Lang::getLocale()] }}</div>
            @endforeach
            <a class="e-button__news-short" href="">{{Lang::get('site.common.more')}}</a>
        </div>
    </div>
    <script type="application/ld+json">
                {
                  "@context": "http://schema.org",
                  "@type": "Organization",
                  "url" : "http://www.tenerifecenter.com",
                  "name": "VYM Canarias",
                  "legalName": "VYM Canarias",
                  "address": {
                    "@type": "PostalAddress",
                    "addressLocality": "Tenerife, Spain",
                    "postalCode": " ",
                    "streetAddress": "Las Americas"
                  },
                  "email": "",
                  "faxNumber": "+34 922 713 395",
                  "telephone":  [
                      "+34 922 787 210",
                      "+34 922 719 553",
                      "+34 922 717 663",
                      "+34 922 713 395"
                  ],
                  "logo": "b-logo.png",
                  "member": [
                    {
                      "@type": "Organization"
                    }
                  ],
                  "employee": [
                      {
                          "@type": "Person",
                          "email": "vym.margarita@gmail.com",
                          "image": "p1.png",
                          "jobTitle": "Director",
                          "name": "Margarita Anikyeva",
                          "telephone": "+34 661 259 125"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.russel@gmail.com",
                          "image": "p2.png",
                          "jobTitle": "Director",
                          "name": "Russel Weightman",
                          "telephone": "+34 992 713 395"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.john@gmail.com",
                          "image": "p3.png",
                          "jobTitle": "Sales Manager",
                          "name": "John Doe",
                          "telephone": "+34 922 713 595"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.mary@gmail.com",
                          "image": "p4.png",
                          "jobTitle": "Sales Manager",
                          "name": "Maria Smith",
                          "telephone": "+34 922 713 695"
                      },
                      {
                          "@type": "Person",
                          "email": "vym.megan@gmail.com",
                          "image": "p5.png",
                          "jobTitle": "Office Manager",
                          "name": "Megan Fox",
                          "telephone": "+34 922 713 772"
                      },
                         {
                          "@type": "Person",
                          "email": "vym.emma@gmail.com",
                          "image": "p6.png",
                          "jobTitle": "Office Manager",
                          "name": "Emma Watson",
                          "telephone": "+34 922 713 872"
                      },
                                            {
                          "@type": "Person",
                          "email": "vym.rose@gmail.com",
                          "image": "p7.png",
                          "jobTitle": "Office Manager",
                          "name": "Rose Byrne",
                          "telephone": "+34 661 720 872"
                      }
                  ]
          }
    </script>
@stop

@section('javascript')
    <script type="text/javascript" src="/includes/index-map.js"></script>
    <script type="text/javascript" src="/includes/scripts.js"></script>

    <script>
        Select.init({
            selector: '.js-e-choose-value',
            className: 'select-theme-default'
        });
        Select.init({
            selector: '.e-search-action',
            className: 'select-theme-action'
        });
        Select.init({
            selector: '.e-time__search',
            className: 'select-theme-time'
        });
    </script>
@stop