@extends('layouts.site')

@section('head')
@stop

@section('content')
    <div class="l-about">
        <div class="l-inner__about">
            @include('parts.about-menu')
            <span class="e-aboutus-title">About Us</span>
            <div class="b-offices-about__about">
                <div class="e-head__offices-about">Our offices.</div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.  Aenean massa. Cum sociis natoque
                    penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                    Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.
                    Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean
                    imperdiet.</p>
            </div>
            <div class="b-offices__about">
                <div class="b-office__offices">
                    <img class="e-photo__office" src="/storage/victoria.png" alt="">
                    <div class="b-office-info__office">
                        <div class="e-name__office-info">VYM Canarias Victoria</div>
                        <div class="e-comment__office-info">Commercial center Victoria Tenerife Sur</div>
                        <div class="e-address__office-info">Calle Republica de Panama 1, C.C.
                            Victoriae Tenerife Sur, local 1-1,
                            Urb. Las Americas, Adeje  38670</div>
                        <div class="b-mail__office-info"><span class="e-value__email">email:</span>vym.margarita@gmail.com</div>
                        <div class="b-skype__office-info"><span class="e-value__skype">skype:</span>favorit-main</div>
                        <div class="b-phone-office__office-info"><span class="e-value__phone-office">office: </span>+34 922 787 210</div>
                        <div class="b-phone-mobile__office-info"><span class="e-value__phone-mobile">mobile: </span>+34 661 259 125</div>
                    </div>
                </div>
                <div class="b-office__offices">
                    <img class="e-photo__office" src="/storage/beril.png" alt="">
                    <div class="b-office-info__office">
                        <div class="e-name__office-info">VYM Canarias, El Beril, La Caleta</div>
                        <div class="e-address__office-info">Local 5, Calle El Beril, La Caleta,
                            Adeje, Santa Cruz de Tenerife</div>
                        <div class="b-mail__office-info"><span class="e-value__email">email:</span>vym.rent@gmail.com</div>
                        <div class="b-skype__office-info"><span class="e-value__skype">skype:</span>vym_rent</div>
                        <div class="b-phone-office__office-info"><span class="e-value__phone-office">office: </span>+34 922 719 553</div>
                        <div class="b-phone-mobile__office-info"><span class="e-value__phone-mobile">mobile: </span>+34 687 698 204</div>
                    </div>
                </div>
                <div class="b-office__offices">
                    <img class="e-photo__office" src="/storage/para.png" alt="">
                    <div class="b-office-info__office">
                        <div class="e-name__office-info">VYM Canarias, Playa Paraiso</div>
                        <div class="e-address__office-info">Local 5, Edif. Sol Paraiso, Calle El
                            Aljibe, 11, Playa Paraiso, 38860,
                            Santa Cruz de Tenerife</div>
                        <div class="b-mail__office-info"><span class="e-value__email">email:</span>vym.sale@gmail.com</div>
                        <div class="b-skype__office-info"><span class="e-value__skype">skype:</span>vym.sale</div>
                        <div class="b-phone-office__office-info"><span class="e-value__phone-office">office: </span>+34 922 713 395</div>
                        <div class="b-phone-mobile__office-info"><span class="e-value__phone-mobile">mobile: </span>+34 635 881 888</div>
                    </div>
                </div>
                <div class="e-map__offices">Offices on map</div>
            </div>
        </div>
    </div>
    <div class="l-map">
        <div class="b-map__index"></div>
    </div>
@stop

@section('javascript')
    <script src="/includes/index-map.js"></script>
    <script src="/includes/scripts.js"></script>
@stop