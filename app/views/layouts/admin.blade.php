<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="stylesheet" href="/assets/stylesheets/admin.css">
        <script src="//maps.googleapis.com/maps/api/js?libraries=places&sensor=false&region=es&language=es"></script>
        {{--<script src="/assets/javascript/admin_old.js"></script>--}}
        <script src="/assets/javascript/admin-libs.js"></script>
        <script src="/assets/javascript/angular-file-upload.min.js"></script>
        <script src="/assets/javascript/admin.js"></script>

        <title>Tenerifecenter</title>

        <style>
            html, body {height: 100%;}
            #wrapper {min-height: 100%; height: auto !important; height: 100%; margin: 0 auto -60px;}
            #push {height: 80px;}
            #footer {height: 60px; background-color: #f5f5f5;}
            @media (max-width: 767px) {
                #footer {margin-left: -20px; margin-right: -20px; padding-left: 20px; padding-right: 20px;}
            }
        </style>
    </head>

    <body>
        <div id="wrapper">

            @yield('content')

            <div id="push"></div>
        </div>

        <div id="footer">
            <div class="container" style="padding-top: 20px;">
                <p class="muted credit">&copy; 2014</p>
            </div>
        </div>

    </body>


</html>













