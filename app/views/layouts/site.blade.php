<!DOCTYPE html>
<html lang="en" ng-app="Tenerifecenter">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <base href="/{{ App::getLocale() }}/">

        {{--<script src="/assets/javascript/site.js"></script>--}}

        <title>Tenerifecenter</title>

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100&subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" type="text/css" href="/assets/stylesheets/site-libs.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/includes/style.css">
        @yield('head')
    </head>

    <body ng-controller="HomeController">
        @include('common.header_site')
        @yield('content')
        @include('common.footer_site')
        <script type="text/javascript" src="/assets/javascript/site-libs.js"></script>
        <script type="text/javascript" src="/assets/javascript/site.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
        @yield('javascript')
    </body>
</html>