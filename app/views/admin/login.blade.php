<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link rel="stylesheet" href="/assets/stylesheets/admin.css">
    <script src="//maps.googleapis.com/maps/api/js?libraries=places&sensor=false&region=es&language=es"></script>
    <script src="/assets/javascript/admin.js"></script>

    <title>Tenerifecenter</title>
</head>

<body class="container">

    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6">

            <form class="form-horizontal" role="form" method="post" style="margin-top: 150px;">
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="btn-group" role="group">
                            <a href="/admin/ru/login" type="button" class="btn btn-default {{ App::getLocale() == 'ru' ? 'active' : '' }}"><img src="/assets/images/flags/ru.png" /></a>
                            <a href="/admin/es/login" type="button" class="btn btn-default {{ App::getLocale() == 'es' ? 'active' : '' }}"><img src="/assets/images/flags/es.png" /></a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label">{{ trans('admin.login.email') }}</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" name="email" id="email" value="{{{ Input::get('email') }}}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-4 control-label">{{ trans('admin.login.password') }}</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password" id="password" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ Input::get('remember') ? 'checked="checked"' : '' }} /> {{ trans('admin.login.memorize') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <input type="submit" class="btn btn-default btn-success btn-block" value="{{ trans('admin.login.login') }}" />
                    </div>
                </div>
            </form>

        </div>
    </div>

</body>


</html>













