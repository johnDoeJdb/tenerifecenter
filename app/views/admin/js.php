<script>

    function initUploader($scope, object, type, FileUploader) {
        $scope.uploader = new FileUploader();
        $scope.uploader.url = '/admin/api/images/save?type=' + type;
        $scope.uploader.queueLimit = 1;
        $scope.uploader.autoUpload = true;
        $scope.uploader.removeAfterUpload = true;
        $scope.uploader.onBeforeUploadItem = function(item) {
            $scope.upload.in_process = true;
            $scope.upload.progress = 0;
        };
        $scope.uploader.onProgressItem = function(item, progress) {
            $scope.upload.in_process = true;
            $scope.upload.progress = progress;
        };
        $scope.uploader.onSuccessItem = function(item, data) {
            $scope.upload.in_process = false;
            object.image = data.path;
        };
    }

    function initMap($scope, object, noAddress) {
        var map;
        var mapOptions = {
            disableDefaultUI: true,
            zoomControl: true,
            scaleControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            }
        };
        var center;
        var marker;

        if (!noAddress) {
            var $autocomplete = angular.element(document.getElementById('autocomplete'));

            var autocomplete = new google.maps.places.Autocomplete(
                $autocomplete[0],
                {
                    types: ['geocode'],
                    componentRestrictions: {country: 'es'},
                    // tenerife bounds
                    bounds: new google.maps.LatLngBounds(
                        new google.maps.LatLng(27.96650784, -17.03157663),
                        new google.maps.LatLng(28.60502008, -16.04555368)
                    )
                }
            );

            if (object.address) {
                $autocomplete.val(object.address);
            }

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                marker.setPosition(place.geometry.location);

                object.lat = place.geometry.location.lat();
                object.lng = place.geometry.location.lng();
                object.address = place.formatted_address;
                $scope.$apply();
            });
        }


        if (object.lat && object.lng) {
            center = new google.maps.LatLng(object.lat, object.lng);
            mapOptions.zoom = 17;
        }
        else {
            center = new google.maps.LatLng(28.0629, -16.7239);
            mapOptions.zoom = 13;
        }

        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        setTimeout(function() {
            google.maps.event.trigger(map, 'resize');
            map.setZoom(map.getZoom());
            map.setCenter(center);
        }, 100);

        var icon = new google.maps.MarkerImage(
            'http://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
            new google.maps.Size(71, 71),
            new google.maps.Point(0, 0),
            new google.maps.Point(17, 34),
            new google.maps.Size(35, 35)
        );

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            icon: icon
        });

        if (object.lat && object.lng) {
            marker.setPosition(center);
        }

        google.maps.event.addListener(map, 'click', function(event) {
            marker.setPosition(event.latLng);
            object.lat = event.latLng.lat();
            object.lng = event.latLng.lng();
            $scope.$apply();
        });
        google.maps.event.addListener(marker, 'position_changed', function() {
            var position = marker.getPosition();
            object.lat = position.lat();
            object.lng = position.lng();
            $scope.$apply();
        });

        // если у объекта не поставлена точка,
        // а пользователь выбрал город, у которого есть координаты
        // подвинем карту в указанный регион
        $scope.findOnMap = function() {
            if (!object.place_id || object.lat) {
                return;
            }
            angular.forEach($scope.places, function(place) {
                if (place.id == object.place_id && place.lat) {
                    var center = new google.maps.LatLng(place.lat, place.lng);
                    map.setCenter(center);
                    map.setZoom(14);
                }
            });
        };
    }

    function sortTree(items) {
        var map = {}, node, roots = [];
        for (var i = 0; i < items.length; i += 1) {
            node = items[i];
            node.children = [];
            map[node.id] = i;
            if (node.parent_id) {
                if (!items[map[node.parent_id]]) {
                    continue;
                }
                items[map[node.parent_id]].children.push(node);
            } else {
                roots.push(node);
            }
        }

        var pages = [];
        function iterate(objects, nesting) {
            for (var o in objects) {
                objects[o].nesting = nesting;
                pages.push(objects[o]);
                if (objects[o].children.length) {
                    iterate(objects[o].children, nesting + 1);
                }
            }
        }

        iterate(roots, 0);

        return pages;
    }

    var routes = [
        ['/places',                           'places',    'PlacesController',        'places'],
        ['/places/create/:parentId?',         'places',    'PlacesEditController',    'places-edit'],
        ['/places/edit/:placeId/:lang?',      'places',    'PlacesEditController',    'places-edit'],

        ['/objects',                          'objects',   'ObjectsController',       'objects'],
        ['/objects/create',                   'objects',   'ObjectsEditController',   'objects-edit'],
        ['/objects/edit/:objectId/:lang?',    'objects',   'ObjectsEditController',   'objects-edit'],

        ['/news',                             'news',      'NewsController',          'news'],
        ['/news/create',                      'news',      'NewsEditController',      'news-edit'],
        ['/news/edit/:newsId/:lang?',         'news',      'NewsEditController',      'news-edit'],

        ['/tags',                             'tags',      'TagsController',          'tags'],
        ['/tags/create/:parentId?',           'tags',      'TagsEditController',      'tags-edit'],
        ['/tags/edit/:tagId/:lang?',          'tags',      'TagsEditController',      'tags-edit'],

        ['/complexes',                        'complexes', 'ComplexesController',     'complexes'],
        ['/complexes/create',                 'complexes', 'ComplexesEditController', 'complexes-edit'],
        ['/complexes/edit/:complexId/:lang?', 'complexes', 'ComplexesEditController', 'complexes-edit'],

        ['/banners',                          'banners',   'BannersController',       'banners'],
        ['/banners/create',                   'banners',   'BannersEditController',   'banners-edit'],
        ['/banners/edit/:bannerId',           'banners',   'BannersEditController',   'banners-edit'],

        ['/offices',                          'offices',   'OfficesController',       'offices'],
        ['/offices/create',                   'offices',   'OfficesEditController',   'offices-edit'],
        ['/offices/edit/:officeId/:lang?',    'offices',   'OfficesEditController',   'offices-edit'],

        ['/managers',                         'managers',  'ManagersController',      'managers'],
        ['/managers/create',                  'managers',  'ManagersEditController',  'managers-edit'],
        ['/managers/edit/:managerId',         'managers',  'ManagersEditController',  'managers-edit'],

        ['/pages',                            'pages',     'PagesController',         'pages'],
        ['/pages/create/:parentId?',          'pages',     'PagesEditController',     'pages-edit'],
        ['/pages/edit/:pageId/:lang?',        'pages',     'PagesEditController',     'pages-edit']
    ];

    angular
        .module('Admin', ['ui.bootstrap', 'ngRoute', 'ngMask', 'angularFileUpload', 'angular-redactor', 'angularUtils.directives.dirPagination'])
        .config(['$routeProvider', 'redactorOptions', 'datepickerConfig', 'datepickerPopupConfig', function($routeProvider, redactorOptions, datepickerConfig, datepickerPopupConfig) {
            redactorOptions.shortcuts = false;
            redactorOptions.plugins = ['table', 'imagemanager'];
            redactorOptions.lang = 'ru';
            redactorOptions.imageUpload = '/admin/api/images/save?type=editor';
            redactorOptions.imageManagerJson = '/admin/api/images';

            datepickerConfig.showWeeks = false;
            datepickerConfig.maxMode = 'day';
            datepickerConfig.startingDay = 1;
            datepickerPopupConfig.showButtonBar = false;

            routes.forEach(function(item) {
                $routeProvider.when(item[0], {
                    module: item[1],
                    controller: item[2],
                    templateUrl: '/assets/templates/'+ item[3] +'.html',
                    reloadOnSearch: false
                });
            });
        }])

        .factory('api', ['$http', function($http) {
            function response(method, resource, args, cb) {
                if (typeof args == 'function') {
                    cb = args;
                    args = null;
                }
                if (!args) {
                    args = {};
                }
                var data = {};
                if (method == 'get') {
                    data = {params: args};
                }
                else {
                    data = args;
                }
                $http[method]('/admin/api/'+resource, data)
                    .success(function(response) {
                        cb(null, response);
                    })
                    .error(function(response) {
                        cb(response);
                    });
            }
            return {
                get: function(resource, args, cb) {
                    response.call(this, 'get', resource, args, cb);
                },
                post: function(resource, args, cb) {
                    response.call(this, 'post', resource, args, cb);
                }
            };

        }])

        .controller('MainController', ['$scope', '$route', 'api', function($scope, $route, api) {
            $scope.$route = $route;
            $scope.translationAvailable = false;
            $scope.translationProcess = {};
            $scope.translations = {};
            $scope.langs = ['ru', 'en', 'es', 'de'];
            $scope.lang = 'ru';
            $scope.modules = [
                'objects',
                'news',
                'pages',
                'tags',
                'places',
                'complexes',
                'banners',
                'offices',
                'managers'
            ];
            $scope.langIs = function(test) {
                return $scope.lang == test;
            };
            $scope.setLang = function(lang) {
                $scope.lang = lang;
            };
            $scope.translate = function(from, texts, cb) {
                var lang = angular.copy($scope.lang);
                var params = {
                    from: from,
                    to: lang,
                    texts: texts
                };
                $scope.translationProcess[lang] = true;
                api.post('translate', params, function(err, response) {
                    $scope.translationProcess[lang] = false;
                    if (err) return $scope.apiError(err);
                    cb(response.data, lang);
                });
            };
            $scope.getName = function(object) {
                if (!object) {
                    return '';
                }
                if (object['name-' + $scope.lang]) {
                    return object['name-' + $scope.lang];
                }
                for (var i = 0; i < $scope.langs.length; i++) {
                    if (object['name-' + $scope.langs[i].id]) {
                        return object['name-' + $scope.langs[i].id];
                    }
                }
                if (object.name) {
                    return object.name;
                }
                return 'Без названия';
            };

            $scope.apiError = function(error) {
                console.log(error);
            };

            $scope.trans = function(token) {
                if ($scope.translations[token]) {
                    return $scope.translations[token];
                }
                return token;
            };

            init($scope);

        }])

        .controller('PlacesController', ['$scope', 'api', function($scope, api) {

            $scope.places = [];

            api.get('places', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.places = response.data;
            });
        }])

        .controller('PlacesEditController', ['$scope', '$routeParams', '$location', 'api', function($scope, $routeParams, $location, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.place = {};
            $scope.breadcrumbs = [{
                url: '#/places',
                name: 'Места'
            }];

            if ($routeParams.placeId || $routeParams.parentId) {
                api.get('places/get', {id: $routeParams.placeId || $routeParams.parentId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    response.path.forEach(function(place) {
                        place.url = '#/places/edit/' + place.id;
                        $scope.breadcrumbs.push(place);
                    });
                    if ($routeParams.placeId) {
                        $scope.place = response.place;
                    }
                    else {
                        $scope.place = {
                            parent_id: response.place.id
                        };
                        $scope.breadcrumbs.push(response.place);
                    }
                    $scope.breadcrumbs.push($scope.place);
                    initMap($scope, $scope.place, true);
                });
            }
            else {
                $scope.breadcrumbs.push($scope.place);
                initMap($scope, $scope.place, true);
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.place['name-'+from],
                    description: $scope.place['description-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.place['name-'+lang] = response.name;
                    $scope.place['description-'+lang] = response.description;
                });
            };

            $scope.save = function() {
                api.post('places/save', $scope.place, function(err, response) {
                    if (err) return $scope.apiError(err);
                    if (response.id) {
                        $scope.place.id = response.id;
                    }
                    $location.path('/places');
                });
            }


        }])

        .controller('ObjectsController', ['$scope', '$location', '$timeout', 'api', function($scope, $location, $timeout, api) {

            $scope.lang = 'ru';
            $scope.search = $location.search();
            $scope.search.per_page = 20;
            $scope.search.page = parseInt($scope.search.page) || 1;

            if ($scope.search.manager) {
                $scope.search.manager = parseInt($scope.search.manager);
            }

            api.get('objects/environment', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.managers = response.managers;
            });

            var search = function(filters) {
                api.get('objects', filters, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.objects = response.data;
                    $scope.total = response.total;
                    $scope.search.page = response.params.page;
                });
            };

            $scope.$watch('search.page', function(current, old) {
                if (current == old) {
                    return;
                }
                $scope.changeSearch('page');
            });

            var searchChangeTimeout = null;
            $scope.changeSearch = function(key) {
                if (key != 'page') {
                    $scope.search.page = 1;
                }
                $location.search(key, $scope.search[key]).replace();

                if (key == 'query') {
                    if (searchChangeTimeout) {
                        $timeout.cancel(searchChangeTimeout);
                    }
                    searchChangeTimeout = $timeout(function() {
                        search($scope.search);
                    }, 350);
                    return;
                }

                search($scope.search);
            };

            $scope.changeSearch('page');

        }])

        .controller('ObjectsEditController', ['$scope', '$routeParams', '$location', 'FileUploader', 'api', function($scope, $routeParams, $location, FileUploader, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.breadcrumbs = [{
                url: '#/objects',
                name: 'Объекты'
            }];

            $scope.uploader = new FileUploader();
            $scope.uploader.url = '/admin/api/images/save?type=object';
            $scope.uploader.autoUpload = true;
            $scope.uploader.removeAfterUpload = true;
            $scope.uploader.onSuccessItem = function(item, data) {
                $scope.object.images.push(data);
            };

            $scope.where = 'complex';
            $scope.tags = [];
            $scope.complexes = [];
            $scope.places = [];
            $scope.managers = [];
            $scope.tagId = {};
            $scope.object = {
                images: [],
                tags: []
            };

            $scope.$watch('step', function(step) {
                if (step == 2) {
                    $scope.initMap();
                }
            });

            $scope.initMap = function() {
                initMap($scope, $scope.object);
                return true;
            };

            var updateTagStatus = function() {
                if (!$scope.tags || !$scope.object) {
                    return;
                }
                angular.forEach($scope.tags, function(tag) {
                    if (!tag.group) {
                        tag.checked = $scope.object.tags.indexOf(tag.id) != -1;
                    }
                    else {
                        angular.forEach(tag.options, function(subtag) {
                            if ($scope.object.tags.indexOf(subtag.id) != -1) {
                                tag.value = subtag.id;
                                return false;
                            }
                        });
                    }
                });
            };

            api.get('objects/get/environment', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.places = response.places;
                $scope.complexes = response.complexes;
                $scope.managers = response.managers;
                $scope.tags = response.tags;
                $scope.tagId = response.tagId;
                updateTagStatus();
            });

            if ($routeParams.objectId) {
                api.get('objects/get', {id: $routeParams.objectId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.object = response.data;
                    $scope.images = response.images;
                    $scope.breadcrumbs.push({
                        name: $scope.object.public_id ? ($scope.object.for == 'rent' ? 'VR ' : 'VS ') + $scope.object.public_id : $scope.object.id
                    });
                    $scope.where = $scope.object.complex_id ? 'complex' : 'address';
                    $scope.initMap();
                    updateTagStatus();
                });
            }
            else {
                $scope.breadcrumbs.push({
                    name: 'Новый объект'
                });
                $scope.initMap();
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.object['name-'+from],
                    teaser: $scope.object['teaser-'+from],
                    description: $scope.object['description-'+from],
                    view: $scope.object['view-'+from],
                    furniture: $scope.object['furniture-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.object['name-'+lang] = response.name;
                    $scope.object['teaser-'+lang] = response.teaser;
                    $scope.object['description-'+lang] = response.description;
                    $scope.object['view-'+lang] = response.view;
                    $scope.object['furniture-'+lang] = response.furniture;
                });
            };

            $scope.save = function() {
                var tags = [];
                angular.forEach($scope.tags, function(tag) {
                    if (tag.value) {
                        tags.push(parseInt(tag.value));
                    }
                    else if (tag.checked) {
                        tags.push(tag.id);
                    }
                });
                $scope.object.tags = tags;
                api.post('objects/save', $scope.object, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/objects');
                });
            };

            $scope.imageRemove = function(image) {
                $scope.object.images.splice($scope.object.images.indexOf(image), 1);
            };

            $scope.imageLeft = function(image) {
                var index = $scope.object.images.indexOf(image);
                var left = $scope.object.images[index - 1];
                $scope.object.images[index - 1] = image;
                $scope.object.images[index] = left;
            };

            $scope.imageRight = function(image) {
                var index = $scope.object.images.indexOf(image);
                var right = $scope.object.images[index + 1];
                $scope.object.images[index + 1] = image;
                $scope.object.images[index] = right;
            };

            $scope.getTags = function(id, exclude) {
                var arr = true;
                if (typeof id != 'array') {
                    id = [id];
                    arr = false;
                }
                var result = [];
                angular.forEach($scope.tags, function(tag) {
                    if (id.indexOf(tag.id) != -1) {
                        if (!exclude) {
                            result.push(tag);
                        }
                    }
                    else if (exclude) {
                        result.push(tag);
                    }
                });
                console.log(!arr && !exclude ? result[0] : result);
                return !arr && !exclude ? result[0] : result;
            };

            $scope.changeWhere = function() {
                if ($scope.where == 'complex') {
                    $scope.object.lat = null;
                    $scope.object.lng = null;
                    $scope.object.address = null;
                    $scope.object.place_id = null;
                }
                else {
                    $scope.object.complex_id = null;
                    initMap($scope, $scope.object);
                }
            };

        }])



        .controller('TagsController', ['$scope', 'api', function($scope, api) {
            $scope.tags = [];

            api.get('tags', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.tags = response.data;
            });
        }])
        .controller('TagsEditController', ['$scope', '$routeParams', '$location', 'api', function($scope, $routeParams, $location, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.tag = {
                group: 0
            };
            $scope.breadcrumbs = [{
                url: '#/tags',
                name: 'Теги'
            }];

            if ($routeParams.tagId || $routeParams.parentId) {
                api.get('tags/get', {id: $routeParams.tagId || $routeParams.parentId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    response.path.forEach(function(tag) {
                        tag.url = '#/tags/edit/' + tag.id;
                        $scope.breadcrumbs.push(tag);
                    });
                    if ($routeParams.tagId) {
                        $scope.tag = response.tag;
                    }
                    else {
                        $scope.tag = {
                            parent_id: response.tag.id
                        };
                        $scope.breadcrumbs.push(response.tag);
                    }
                    $scope.breadcrumbs.push($scope.tag);
                });
            }
            else {
                $scope.breadcrumbs.push($scope.tag);
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.tag['name-'+from],
                    description: $scope.tag['description-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.tag['name-'+lang] = response.name;
                    $scope.tag['description-'+lang] = response.description;
                });
            };

            $scope.save = function() {
                api.post('tags/save', $scope.tag, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/tags');
                });
            };

        }])

        .controller('PagesController', ['$scope', 'api', function($scope, api) {
            $scope.pages = [];

            api.get('pages', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.pages = sortTree(response.data);
            });

        }])
        .controller('PagesEditController', ['$scope', '$routeParams', '$location', 'api', function($scope, $routeParams, $location, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.page = {};
            $scope.breadcrumbs = [];

            if ($routeParams.pageId || $routeParams.parentId) {
                api.get('pages/get', {id: $routeParams.pageId || $routeParams.parentId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    if ($routeParams.pageId) {
                        $scope.page = response.data;
                    }
                    else {
                        $scope.page = {
                            parent_id: response.data.id
                        };
                    }
                    $scope.breadcrumbs = [{
                        url: '#/pages',
                        name: 'Страницы'
                    }];
                    response.path.forEach(function(page) {
                        page.url = '#/pages/edit/' + page.id;
                        $scope.breadcrumbs.push(page);
                    });
                    if ($routeParams.parentId) {
                        $scope.breadcrumbs.push(response.page);
                    }
                    $scope.breadcrumbs.push($scope.page);
                });
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.page['name-'+from],
                    keywords: $scope.page['keywords-'+from],
                    description: $scope.page['description-'+from],
                    content: $scope.page['content-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.page['name-'+lang] = response.name;
                    $scope.page['keywords-'+lang] = response.keywords;
                    $scope.page['description-'+lang] = response.description;
                    $scope.page['content-'+lang] = response.content;
                });
            };

            $scope.save = function() {
                api.post('pages/save', $scope.page, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/pages');
                });
            }
        }])

        .controller('ManagersController', ['$scope', 'api', function($scope, api) {
            $scope.managers = [];

            api.get('managers', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.managers = response.data;
            });
        }])
        .controller('ManagersEditController', ['$scope', '$routeParams', '$location', 'FileUploader', 'api', function($scope, $routeParams, $location, FileUploader, api) {
            $scope.manager = {
                active: 1,
                role: 'manager'
            };
            $scope.roles = [];
            $scope.offices = [];

            $scope.upload = {
                in_process: false,
                progress: 0
            };

            initUploader($scope, $scope.manager, 'manager', FileUploader);

            api.get('managers/environment', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.roles = response.roles;
                $scope.offices = response.offices;
            });

            if ($routeParams.managerId) {
                api.get('managers/get', {id: $routeParams.managerId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.manager = response.data;
                });
            }

            $scope.save = function() {
                api.post('managers/save', $scope.manager, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/managers');
                });
            };

        }])

        .controller('NewsController', ['$scope', '$location', 'api', function($scope, $location, api) {

            $scope.search = $location.search();
            $scope.search.per_page = 20;
            $scope.search.page = parseInt($scope.search.page) || 1;

            var search = function(filters) {
                api.get('news', filters, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.news = response.data;
                    $scope.total = response.total;
                    $scope.search.page = response.params.page;
                });
            };

            $scope.$watch('search.page', function(current, old) {
                if (current == old) {
                    return;
                }
                $scope.changeSearch('page');
            });

            $scope.changeSearch = function(key) {
                if (key != 'page') {
                    $scope.search.page = 1;
                }
                $location.search(key, $scope.search[key]).replace();

                search($scope.search);
            };

            $scope.changeSearch('page');

            $scope.getDate = function(seconds) {
                if (!seconds) {
                    return '';
                }
                return moment(seconds * 1000).format('ll');
            }

        }])

        .controller('NewsEditController', ['$scope', '$routeParams', '$location', 'api', function($scope, $routeParams, $location, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.news = {};

            $scope.calendarOpened = false;

            if ($routeParams.newsId) {
                api.get('news/get', {id: $routeParams.newsId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    if (response.data.published_at) {
                        response.data.published_at = new Date(response.data.published_at * 1000).toISOString();
                    }
                    $scope.news = response.data;
                });
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.news['name-'+from],
                    teaser: $scope.news['teaser-'+from],
                    content: $scope.news['content-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.news['name-'+lang] = response.name;
                    $scope.news['teaser-'+lang] = response.teaser;
                    $scope.news['content-'+lang] = response.content;
                });
            };

            $scope.save = function() {
                var news = angular.copy($scope.news);
                if (news.published_at) {
                    news.published_at = +new Date(news.published_at) / 1000;
                }
                api.post('news/save', news, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/news');
                });
            };

            $scope.openCalendar = function($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.calendarOpened = true;
            };
        }])

        .controller('OfficesController', ['$scope', 'api', function($scope, api) {

            $scope.offices = [];

            api.get('offices', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.offices = response.data;
            });
        }])

        .controller('OfficesEditController', ['$scope', '$routeParams', '$location', 'FileUploader', 'api', function($scope, $routeParams, $location, FileUploader, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.breadcrumbs = [{
                url: '#/offices',
                name: 'Офисы'
            }];

            $scope.places = [];
            $scope.office = {};

            initUploader($scope, $scope.office, 'office', FileUploader);

            api.get('offices/get/environment', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.places = response.places;
            });

            if ($routeParams.officeId) {
                api.get('offices/get', {id: $routeParams.officeId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.office = response.office;
                    $scope.breadcrumbs.push($scope.office);
                    initMap($scope, $scope.office);
                });
            }
            else {
                $scope.breadcrumbs.push($scope.office);
                initMap($scope, $scope.office);
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.office['name-'+from],
                    description: $scope.office['description-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.office['name-'+lang] = response.name;
                    $scope.office['description-'+lang] = response.description;
                });
            };

            $scope.save = function() {
                api.post('offices/save', $scope.office, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/offices');
                });
            };

        }])

        .controller('ComplexesController', ['$scope', 'api', function($scope, api) {

            $scope.complexes = [];

            api.get('complexes', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.complexes = response.data;
            });
        }])

        .controller('ComplexesEditController', ['$scope', '$routeParams', '$location', 'FileUploader', 'api', function($scope, $routeParams, $location, FileUploader, api) {
            $scope.$parent.lang = $routeParams.lang || $scope.lang;

            $scope.breadcrumbs = [{
                url: '#/complexes',
                name: 'Комплексы'
            }];

            $scope.places = [];
            $scope.complex = {};

            initUploader($scope, $scope.complex, 'complex', FileUploader);

            api.get('complexes/get/environment', function(err, response) {
                if (err) return $scope.apiError(err);
                $scope.places = response.places;
            });

            if ($routeParams.complexId) {
                api.get('complexes/get', {id: $routeParams.complexId}, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $scope.complex = response.complex;
                    $scope.breadcrumbs.push($scope.complex);
                    initMap($scope, $scope.complex);
                });
            }
            else {
                $scope.breadcrumbs.push($scope.complex);
                initMap($scope, $scope.complex);
            }

            $scope.translate = function(from) {
                var texts = {
                    name: $scope.complex['name-'+from],
                    description: $scope.complex['description-'+from]
                };
                $scope.$parent.translate(from, texts, function(response, lang) {
                    $scope.complex['name-'+lang] = response.name;
                    $scope.complex['description-'+lang] = response.description;
                });
            };

            $scope.save = function() {
                api.post('complexes/save', $scope.complex, function(err, response) {
                    if (err) return $scope.apiError(err);
                    $location.path('/complexes');
                });
            };

        }])


        .filter('numberFixedLen', function () {
            return function(a,b){
                return (1e4 + a + '').slice(-b);
            }
        });
</script>