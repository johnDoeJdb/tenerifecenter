@extends('layouts.admin')

@section('content')

<div ng-app="Admin" ng-controller="MainController">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a href="#/" class="navbar-brand">{{ trans('admin.site.title') }}</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">{{{ $user->name }}}</a></li>
                <li><a href="{{ route('admin.logout') }}">{{ trans('admin.site.logout') }}</a></li>
                <li><a href="/">{{ trans('admin.site.to_site') }}</a></li>
                <li{{ App::getLocale() == 'ru' ? ' class="active"' : '' }}><a href="/admin/ru/"><img src="/assets/images/flags/ru.png" /></a></li>
                <li{{ App::getLocale() == 'es' ? ' class="active"' : '' }}><a href="/admin/es/"><img src="/assets/images/flags/es.png" /></a></li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="list-group">
                    <a ng-repeat="module in modules" ng-class="{active: $route.current.module == module}" href="#/@{{ module }}" class="list-group-item">@{{ trans('menu.'+module) }}</a>
                </div>
            </div>
            <div class="col-sm-12 col-md-9">
                <div ng-view></div>
            </div>
        </div>
    </div>
</div>

<script>

    moment.locale('{{ App::getLocale() }}');

    function init($scope) {
        $scope.translations = {{ json_encode($translations, JSON_UNESCAPED_UNICODE) }};
    }

</script>

@include('admin.js')

@stop