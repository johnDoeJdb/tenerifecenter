<div class="l-footer">
    <div class="l-footer-top">
        <div class="b-catalog">
            <div class="b-catalog__column">
                <div class="e-catalog-title">{{Lang::get('site.tag.special_offers')}}</div>
                <ul>
                    <li class="e-li__catalog"><a href="" class="e-link__catalog">VIP</a></li>
                    <li class="e-li__catalog"><a href="" class="e-link__catalog">Seafront</a></li>
                    <li class="e-li__catalog"><a href="" class="e-link__catalog">Urgent sale</a></li>
                    <li class="e-li__catalog"><a href="" class="e-link__catalog">Unique properies</a></li>
                </ul>
            </div>
            <div class="b-catalog__column">
                <div class="e-catalog-title">{{Lang::get('site.tag.popular_locations')}}</div>
                <ul>
                    @foreach ($popularPlaces as $key => $place)
                        <li class="e-li__catalog"><a href="/{{Lang::getLocale()}}/search?place={{$key}}" class="e-link__catalog">{{$place}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="b-catalog__column">
                <div class="e-catalog-title">{{Lang::get('site.tag.property_types')}}</div>
                <ul>
                    @foreach ($popularPropertyTypes as $key => $property)
                        <li class="e-li__catalog"><a href="/{{Lang::getLocale()}}/search?type={{$key}}" class="e-link__catalog">{{$property}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="b-allcontacts">
            <div class="b-phones">
                <div class="e-phone-icon__phones"><img src="/includes/e-phone-icon.png" alt=""></div>
                <div class="b-phones__text">
                    <div class="e-allcontacts-title">{{Lang::get('site.contacts.mobile')}}:</div>
                    <div class="e-allcontacts-value">+34 661 259 125</div>
                    <div class="e-allcontacts-title">{{Lang::get('site.contacts.office')}}:</div>
                    <div class="e-allcontacts-value" >+34 922 787 210</div>
                    <div class="e-allcontacts-value" >+34 922 719 553</div>
                    <div class="e-allcontacts-value" >+34 922 717 663</div>
                    <div class="e-allcontacts-value" >+34 922 713 395</div>
                    <div class="e-allcontacts-title">{{Lang::get('site.contacts.fax')}}:</div>
                    <div class="e-allcontacts-value">+34 922 713 395</div>
                </div>
            </div>
            <div class="b-address">
                <div class="e-home-icon__address"><img src="/includes/e-home-icon.png" alt=""></div>
                <div class="b-address__text">
                    <div class="e-value__address">VYM Canarias</div>
                    <div class="e-value__address">C/C "Victoria Tenerife Sur"</div>
                    <div class="e-value__address">Calle Republica de Panama, 1"</div>
                    <div class="e-value__address">Las Americas, Tenerife, Spain</div>
                </div>
            </div>
        </div>
    </div>
    <div class="l-footer-down">
        <div class="b-pop-links">
            <a class="e-pop-link" href="/sale">{{Lang::get('site.menu.sale')}}</a>
            <a class="e-pop-link" href="/rent">{{Lang::get('site.menu.rent')}}</a>
            <a class="e-pop-link" style="cursor: not-allowed" disabled="disabled" href="">{{Lang::get('site.menu.special_offers')}}</a>
            <a class="e-pop-link" style="cursor: not-allowed" disabled="disabled" href="">{{Lang::get('site.menu.investments')}}</a>
            <a class="e-pop-link" style="cursor: not-allowed" disabled="disabled" href="">{{Lang::get('site.menu.tourism')}}</a>
            <a class="e-pop-link" href="/about-team">{{Lang::get('site.menu.about')}}</a>
        </div>
        <div class="e-copyright__footer-down">
            © Copyright 2015 VYM Canarias CIF B-76521145 Все права защищены.
            Копирование материалов нашего сайта разрешено только при обязательном указании прямой ссылки на наш сайт
        </div>
        <div class="b-social-networks">
            <a class="b-facebook__social-networks"  href=""><img  class="e-image__facebook" src="/includes/e-facebook.png" alt=""></a>
            <a class="b-google__social-networks" href=""><img class="e-image__google" src="/includes/e-google.png" alt=""></a>
            <a class="b-vk__social-networks" href=""><img class="e-image__vk" src="/includes/e-vk.png" alt=""></a>
            <a class="b-twitter__social-networks" href=""><img class="e-image__twitter" src="/includes/e-twitter.png" alt=""></a>
            <a class="b-mail__social-networks" href=""><img class="e-image__mail" src="/includes/e-mail.png" alt=""></a>
        </div>
    </div>
</div>