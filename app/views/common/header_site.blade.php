<div class="l-panel">
    <div class="b-contacts">
        <div class="b-contacts__tel">
            <img  class="b-contacts-tel__image" src="/includes/b-contacts-tel.png" alt="">
            <span class="e-contacts-tel__text">+34 992 787 210 </span>
            <span class="e-contacts-tel__text">+34 661 259 125</span>
        </div>
        <a class="b-contacts__mail" href="mailto:info@tenerifecenter.com">
            <img class="b-contacts-mail__image" src="/includes/b-contacts-mail.png" alt="">
            <div class="e-contacts-mail__text">info@tenerifecenter.com</div>
        </a>
        <a class="b-contacts__skype" href="skype:tenerifecenter?call">
            <img  class="b-contacts-skype__image" src="/includes/b-contacts-skype.png" alt="">
            <span class="e-contacts-skype__text">tenerifecenter</span>
        </a>
    </div>
    <div class="b-person">
        {{ Form::select('lang', BladeHelper::getLanguages(), Lang::getLocale(), array('class' => 'b-lang', 'onchange' => 'swapImage()')) }}
        <img class="e-language__image"  src="/includes/b-lang-{{Lang::getLocale()}}.png" alt="">
        <div class="b-user">
            <div class="b-login__user" onclick="show('block')"><img class="e-user__user" src="/includes/b-user.png" alt=""></div>
        </div>
    </div>
    <div onclick="show('none')" class="wrap"></div>
    <div class="b-login__panel">
        <a class="e-close__login" href=""  onclick="show('none')">x</a>
        <div class="e-head__login">Log In</div>
        <input class="b-username__login" placeholder="Username">
        <input class="b-password__login" placeholder="Password">
        <a class="e-button__login" href="">LOG IN</a>
    </div>

</div>
<div class="l-main">
    <div class="b-logo">
        <a @if (Request::path() == '') href="/" @else onclick="javascript:void(0)" href="#" @endif ><img class="e-logo__logo"  src="/includes/b-logo.png" alt=""></a>
    </div>
    <div class="l-main-menu">
        <div class="b-main-menu">
            <a class="e-main-about @if (Request::path() == 'about-team' || Request::path() == 'about-offices' || Request::path() == 'about-jobs' || Request::path() == 'about-services') js-e-main-menu-current @endif" href="/about-team">{{Lang::get('site.menu.about');}}</a>
            <a class="e-main-tourism @if (Request::path() == 'tourism') js-e-main-menu-current @endif" href="{{--/tourism--}}javascript:void(0);"> {{Lang::get('site.menu.tourism');}} </a>
            <a class="e-main-invest @if (Request::path() == 'investments') js-e-main-menu-current @endif" href="{{--/investments--}}javascript:void(0);"> {{Lang::get('site.menu.investments');}} </a>
            <a class="e-main-specoff @if (Request::path() == 'special-offers') js-e-main-menu-current @endif" href="{{--/special-offers--}}javascript:void(0);"> {{Lang::get('site.menu.special_offers');}} </a>
            <a class="e-main-rent @if (Request::path() == 'rent') js-e-main-menu-current @endif" href ="/rent">{{Lang::get('site.menu.rent');}}</a>
            <a class="e-main-sale @if (Request::path() == 'sale') js-e-main-menu-current @endif" href="/sale">{{Lang::get('site.menu.sale');}}</a>
        </div>
    </div>
</div>