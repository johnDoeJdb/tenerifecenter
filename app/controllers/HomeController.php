<?php

class HomeController extends BaseController {

    public function changelog() {
        return View::make('site.changelog');
    }

	public function index() {
        $l = App::getLocale();
        $page = Page::find(Config::get('content.main_page_id'));
        $places = Place::getPlacesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());
        $complexes = Complex::getComplexesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $news = News::whereNotNull("name-$l")->orderBy('published_at', 'desc')->limit(8)->get(['id', "name-$l", "teaser-$l", 'published_at']);
        $objects = Tag::getObjectsByType();

		return View::make('site.index')->with([
            'page' => $page,
            'propertyTypes' => $propertyTypes,
            'objects' => $objects,
            'news' => $news,
            'places' => $places,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
            'complexes' => $complexes,
        ]);
	}

    public function search() {
        $l = App::getLocale();

        $tags = Tag::with(['options' => function($query) use (&$l) {
            $query->get(['id', "name-$l as name", 'parent_id']);
        }])
            ->whereNull('parent_id')
            ->where('type', '=', 'feature')
            ->get(['id', "name-$l as name", 'group']);

        $places = Place::getForSelect(App::getLocale());
        $complexes = Complex::get(['id', "name-$l as name", 'place_id']);

        return View::make('site.search')->with([
            'tags' => $tags,
            'tagId' => Config::get('object.tags'),
            'places' => $places,
            'complexes' => $complexes,
        ]);
    }

    public function salePage() {
        $l = App::getLocale();
        $page = Page::find(Config::get('content.main_page_id'));
        $places = Place::getPlacesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());
        $complexes = Complex::getComplexesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $news = News::whereNotNull("name-$l")->orderBy('published_at', 'desc')->limit(8)->get(['id', "name-$l", "teaser-$l", 'published_at']);
        $objects = Tag::getObjectsByType('sale');

        return View::make('site.salePage')->with([
            'page' => $page,
            'propertyTypes' => $propertyTypes,
            'objects' => $objects,
            'news' => $news,
            'places' => $places,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
            'complexes' => $complexes,
        ]);
    }

    public function rentPage() {
        $l = App::getLocale();
        $page = Page::find(Config::get('content.main_page_id'));
        $places = Place::getPlacesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());
        $complexes = Complex::getComplexesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $news = News::whereNotNull("name-$l")->orderBy('published_at', 'desc')->limit(8)->get(['id', "name-$l", "teaser-$l", 'published_at']);
        $objects = Tag::getObjectsByType('rent');

        return View::make('site.rentPage')->with([
            'page' => $page,
            'propertyTypes' => $propertyTypes,
            'objects' => $objects,
            'news' => $news,
            'places' => $places,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
            'complexes' => $complexes,
        ]);
    }

    public function sale($id) {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $complexes = Complex::getComplexesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());
        $objects = Tag::getObjectsByType('sale');
        $object = Object::find($id);
        if ($object) {
            $place = Place::find($object['place_id']);
            $furnitureTag = Tag::where('name-ru', 'Мебелирован')->first();
            $poolTag = Tag::where('name-ru', 'Бассейн')->first();
            $garageTag = Tag::where('name-ru', 'Гараж')->first();
            $viewTag = Tag::where('name-ru', 'Вид')->first();
            $propertyType = $object->tags()->where('parent_id', Config::get('object.tags.type'))->first();
            $furniture = $object->tags()->where('parent_id', $furnitureTag['id'])->first();
            $pool = $object->tags()->where('parent_id', $poolTag['id'])->first();
            $garage = $object->tags()->where('parent_id', $garageTag['id'])->first();
            $view = $object->tags()->where('parent_id', $viewTag['id'])->first();
            $images = $object->images()->get();
            $manager = $object->manager()->get()->first();
        } else {
            return Redirect::to('/');
        }

        return View::make('site.sale')->with([
            'propertyTypes' => $propertyTypes,
            'objects' => $objects,
            'object' => $object,
            'place' => $place,
            'places' => $places,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
            'propertyType' => $propertyType,
            'furniture' => $furniture,
            'pool' => $pool,
            'garage' => $garage,
            'view' => $view,
            'manager' => $manager,
            'images' => $images,
            'complexes' => $complexes,
        ]);
    }

    public function rent($id) {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $complexes = Complex::getComplexesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());
        $objects = Tag::getObjectsByType('rent');
        $object = Object::find($id);
        if ($object) {
            $place = Place::find($object['place_id']);
            $furnitureTag = Tag::where('name-ru', 'Мебелирован')->first();
            $poolTag = Tag::where('name-ru', 'Бассейн')->first();
            $garageTag = Tag::where('name-ru', 'Гараж')->first();
            $viewTag = Tag::where('name-ru', 'Вид')->first();
            $propertyType = $object->tags()->where('parent_id', Config::get('object.tags.type'))->first();
            $furniture = $object->tags()->where('parent_id', $furnitureTag['id'])->first();
            $pool = $object->tags()->where('parent_id', $poolTag['id'])->first();
            $garage = $object->tags()->where('parent_id', $garageTag['id'])->first();
            $view = $object->tags()->where('parent_id', $viewTag['id'])->first();
            $images = $object->images()->get();
        } else {
            return Redirect::to('/');
        }

        return View::make('site.rent')->with([
            'propertyTypes' => $propertyTypes,
            'objects' => $objects,
            'places' => $places,
            'place' => $place,
            'object' => $object,
            'propertyType' => $propertyType,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
            'furniture' => $furniture,
            'pool' => $pool,
            'garage' => $garage,
            'view' => $view,
            'images' => $images,
            'complexes' => $complexes,
        ]);
    }

    public function aboutTeam() {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());

        return View::make('site.about-team')->with([
            'places' => $places,
            'propertyTypes' => $propertyTypes,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
        ]);
    }

    public function aboutOffices() {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());

        return View::make('site.about-offices')->with([
            'places' => $places,
            'propertyTypes' => $propertyTypes,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
        ]);
    }

    public function aboutServices() {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());

        return View::make('site.about-services')->with([
            'places' => $places,
            'propertyTypes' => $propertyTypes,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
        ]);
    }

    public function aboutJobs() {
        $l = App::getLocale();
        $places = Place::getPlacesNames(App::getLocale());
        $propertyTypes = Tag::getPropertyTypesNames(App::getLocale());
        $popularPlaces = Place::getPopularPlacesNames(App::getLocale());
        $popularPropertyTypes = Tag::getPopularPropertyTypesNames(App::getLocale());

        return View::make('site.about-jobs')->with([
            'places' => $places,
            'propertyTypes' => $propertyTypes,
            'popularPlaces' => $popularPlaces,
            'popularPropertyTypes' => $popularPropertyTypes,
        ]);
    }
}
