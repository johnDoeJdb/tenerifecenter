<?php
namespace Admin;

use \Input,
    \Tag;

class TagsController extends \BaseController {

    function all() {
        $tags = Tag::with('options')
            ->whereNull('parent_id')
            ->where('type', '=', 'feature')
            ->get();
        return $this->json([
            'data' => $tags
        ]);
    }

    function get() {
        $id = Input::get('id');

        $tag = Tag::find($id);

        return $this->json([
            'tag' => $tag,
            'path' => $tag->getPath()
        ]);
    }

    function save() {
        $data = Input::all();

        if (!empty($data['id'])) {
            $tag = Tag::find($data['id']);
            $tag->update($data);
        }
        else {
            $tag = new Tag();
            $tag->type = 'feature';
            $tag->parent_id = !empty($data['parent_id']) ? $data['parent_id'] : null;
            $tag->group = !empty($data['group']) && empty($data['parent_id']);
            $tag->fill($data);
            $tag->save();
        }

        return $this->json([]);
    }

}