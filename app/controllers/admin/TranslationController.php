<?php
namespace Admin;

use \Input;

class TranslationController extends \BaseController {

    function translate() {
        $from = Input::get('from');
        $to = Input::get('to');
        $texts = Input::get('texts');

        $query = http_build_query([
            'key' => \Config::get('services.yandex.translator'),
            'lang' => "$from-$to",
            'format' => 'html'
        ]);

        $textQuery = '';
        foreach ($texts as $field => $text) {
            if (!strlen($text)) continue;
            $textQuery .= '&text='.urlencode($text);
        }

        $result = [];
        $response = [];

        if ($textQuery) {

            $context = stream_context_create([
                'http' => [
                    'method' => 'POST',
                    'header' => 'Content-Type: application/x-www-form-urlencoded',
                    'content' => $query.$textQuery,
                ]
            ]);

            $response = file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate', false, $context);

            $response = json_decode($response, true);
            $response = $response['text'];
        }

        foreach ($texts as $field => $text) {
            if (!strlen($text)) {
                $result[$field] = '';
                continue;
            }
            $result[$field] = array_shift($response);
        }

        return $this->json(['data' => $result]);
    }

}