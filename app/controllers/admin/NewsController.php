<?php
namespace Admin;

use \Input,
    \News;

class NewsController extends \BaseController {

    function all() {
        $params = Input::all();
        $total = 0;

        $data = News::getList($params, $total);

        return $this->json([
            'data' => $data,
            'params' => $params,
            'total' => $total
        ]);
    }

    function get() {
        $id = Input::get('id');

        $news = News::find($id);

        return $this->json([
            'data' => $news
        ]);
    }

    function save() {
        $data = Input::all();

        if (!empty($data['id'])) {
            $news = News::find($data['id']);
            $news->update($data);

            return $this->json([]);
        }
        else {
            $news = new News();
            $news->fill($data);
            $news->save();

            return $this->json(['id' => $news->id]);
        }
    }
}