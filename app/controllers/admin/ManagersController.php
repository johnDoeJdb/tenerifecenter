<?php
namespace Admin;

use \User,
    \Input,
    \Validator,
    \Hash,
    \Office;

class ManagersController extends \BaseController {

    function all() {
        return $this->json([
            'data' => User::get()
        ]);
    }

    function get() {
        $id = Input::get('id');

        $user = User::find($id);

        $data = $user->offices()->get();
        $offices = [];
        foreach ($data as $office) {
            $offices[] = $office->id;
        }
        $user->offices = $offices;

        return $this->json([
            'data' => $user
        ]);
    }

    function save() {
        $data = Input::all();

        $offices = [];
        if (isset($data['offices'])) {
            $offices = array_values($data['offices']);
            unset($data['offices']);
        }

        $rules = [
            'active' => ['boolean'],
            'role' => ['required', 'in:admin,manager'],
            'name-en' => ['required', 'min:5'],
            'email' => ['required', 'email', 'unique:users,email'.(!empty($data['id']) ? ",$data[id]" : '')],
        ];

        if (empty($data['id']) || !empty($data['password'])) {
            $rules['password'] = ['required', 'min:4', 'confirmed'];
        }

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return $this->json($validator->messages(), 500);
        }

        unset($data['password_confirmation']);

        if (!empty($data['id'])) {
            $user = User::find($data['id']);
            if (!empty($data['password'])) {
                $user->password = Hash::make($data['password']);
            }
            $user->update($data);
        }
        else {
            $user = new User();
            $user->fill($data);
            $user->password = Hash::make($data['password']);
            $user->save();
        }

        $user->offices()->sync($offices);

        return $this->json([]);
    }

    function getEnvironment() {
        $offices = Office::get();

        return $this->json([
            'roles' => [
                ['role' => 'admin', 'name' => 'Администратор'],
                ['role' => 'manager', 'name' => 'Менеджер'],
            ],
            'offices' => $offices
        ]);
    }

}