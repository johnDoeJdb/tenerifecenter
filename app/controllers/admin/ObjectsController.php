<?php
namespace Admin;

use \App,
    \Auth,
    \Input,
    \Object,
    \Place,
    \Complex,
    \Tag,
    \User;

class ObjectsController extends \BaseController {

    function all() {
        $params = Input::all();
        $total = 0;

        $data = Object::getList($params, $total);

        return $this->json([
            'data' => $data,
            'params' => $params,
            'total' => $total
        ]);
    }

    function get() {
        $id = Input::get('id');

        $object = Object::where('id', '=', $id)->with('images')->first();

        $data = $object->tags()->get();
        $tags = [];
        foreach ($data as $tag) {
            $tags[] = $tag->id;
        }
        $object->tags = $tags;

        return $this->json([
            'data' => $object
        ]);
    }

    function save() {
        $data = Input::all();

        $tags = [];
        if (isset($data['tags'])) {
            $tags = array_values($data['tags']);
            unset($data['tags']);
        }

        $images = [];
        $order = 1;
        if (isset($data['images'])) {
            foreach ($data['images'] as $image) {
                $images[$image['id']] = [
                    'order' => $order++
                ];
            }
            unset($data['images']);
        }

        $action = !empty($data['action']) ? $data['action'] : null;
        unset($data['action']);

        if (!empty($data['id'])) {
            $object = Object::find($data['id']);
            $object->update($data);
        }
        else {
            $object = new Object();
            $object->fill($data);
            $object->manager_id = Auth::user()->id;
            $object->save();
        }

        if ($action == 'publish') {
            $object->publish();
        }
        elseif ($action == 'unpublish') {
            $object->unpublish();
        }

        $object->tags()->sync($tags);
        $object->images()->sync($images);

        return $this->json(['id' => $object->id]);
    }

    function allEnvironment() {
        $managers = \User::all(['id', 'name-en', 'name-ru']);
        return $this->json([
            'managers' => $managers
        ]);
    }

    function getEnvironment() {
        $l = \App::getLocale();
        $places = Place::getForSelect($l);
        $complexes = Complex::get(['id', "name-$l as name"]);
        $managers = User::get(['id', 'name-en', 'name-ru']);
        $tags = Tag::with('options')
            ->whereNull('parent_id')
            ->get();

        return $this->json([
            'tags' => $tags,
            'places' => $places,
            'complexes' => $complexes,
            'managers' => $managers,
            'tagId' => \Config::get('object.tags')
        ]);
    }
}