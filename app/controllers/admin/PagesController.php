<?php
namespace Admin;

use \Input,
    \Page;

class PagesController extends \BaseController {

    function all() {
        return [
            'data' => Page::getList()
        ];
    }

    function get() {
        $id = Input::get('id');

        $page = Page::find($id);

        return $this->json([
            'data' => $page,
            'path' => $page->getPath()
        ]);
    }

    function save() {
        $data = Input::all();

        if (!empty($data['id'])) {
            $page = Page::find($data['id']);
            $page->update($data);

            return $this->json([]);
        }
        else {
            $page = new Page();
            $page->fill($data);
            $page->parent_id = $data['parent_id'];
            $page->save();

            return $this->json(['id' => $page->id]);
        }
    }
}