<?php
namespace Admin;

use \Input,
    \Place,
    \Office;

class OfficesController extends \BaseController {

    function all() {
        $offices = Office::with('place')->get();
        return $this->json([
            'data' => $offices
        ]);
    }

    function get() {
        $id = Input::get('id');

        $office = Office::with('place')->find($id);

        return [
            'office' => $office
        ];
    }

    function getEnvironment() {
        $l = \App::getLocale();
        $places = Place::getForSelect($l);

        return $this->json([
            'places' => $places
        ]);
    }

    function save() {
        $data = Input::all();

        unset($data['place']);

        if (!empty($data['id'])) {
            $office = Office::find($data['id']);
            $office->update($data);
        }
        else {
            $office = new Office();
            $office->fill($data);
            $office->save();
        }

        return $this->json([]);
    }

}