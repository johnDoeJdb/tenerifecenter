<?php
namespace Admin;

use \Input,
    \Place,
    \Complex;

class ComplexesController extends \BaseController {

    function all() {
        $complexes = Complex::with('place')->get();
        return $this->json([
            'data' => $complexes
        ]);
    }

    function get() {
        $id = Input::get('id');

        $complex = Complex::with('place')->find($id);

        return [
            'complex' => $complex
        ];
    }

    function getEnvironment() {
        $l = \App::getLocale();
        $places = Place::getForSelect($l);

        return $this->json([
            'places' => $places
        ]);
    }

    function save() {
        $data = Input::all();

        unset($data['place']);

        if (!empty($data['id'])) {
            $complex = Complex::find($data['id']);
            $complex->update($data);
        }
        else {
            $complex = new Complex();
            $complex->fill($data);
            $complex->save();
        }

        return $this->json([]);
    }

}