<?php
namespace Admin;

use \App,
    \Controller,
    \Request,
    \Input,
    \Auth,
    \View;

class AdminController extends \BaseController {

    public function test() {
        return $this->json([
            'env' => App::environment(),
            'object' => \Config::get('object'),
        ]);
    }

    public function login() {
        if (Request::isMethod('post')) {
            $email = Input::get('email');
            $password = Input::get('password');
            $remember = (bool) Input::get('remember', false);
            if (Auth::attempt(array('email' => $email, 'password' => $password, 'active' => 1), $remember)) {
                return \Redirect::to(route('admin.index'));
            }
        }
        return \View::make('admin.login');
    }

    public function logout() {
        Auth::logout();
        return \Redirect::to(route('admin.login'));
    }

    public function index() {
        $authUser = Auth::user();
        return View::make('admin.index')->with([
            'user' => $authUser,
            'translations' => $this->_getTranslations()
        ]);
    }

    private function _getTranslations() {
        $array = trans('admin');
        $strings = [];

        $recursive_iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));
        foreach ($recursive_iterator as $value) {
            $keys = [];
            foreach (range(0, $recursive_iterator->getDepth()) as $depth) {
                $keys[] = $recursive_iterator->getSubIterator($depth)->key();
            }
            $strings[ join('.', $keys) ] = $value;
        }

        return $strings;
    }
}
