<?php
namespace Admin;

use \Input,
    \Image,
    \Intervention;

class ImagesController extends \BaseController {

	public function all() {
		$images = Image::orderBy('created_at', 'desc')
            ->limit(30)
            ->get(['preview', 'fullsize']);

        $result = [];
        foreach ($images as $image) {
            $result[] = [
                'thumb' => '/object_images/'.$image['preview'],
                'image' => '/object_images/'.$image['fullsize'],
            ];
        }

        return $this->json($result);
	}

    function save() {
        $file = Input::file('file');
        $type = Input::get('type');

        $ext = $file->getClientOriginalExtension();

        switch ($type) {
            case 'object':
                $fullsize = uniqid().'.'.$ext;
                $preview = uniqid().'.'.$ext;

                $img = Intervention::make($file->getRealPath());
                $img->resize(1024, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->insert(public_path('assets/images/watermark.png'), 'bottom-right', 10, 10);
                $img->save(\Config::get('app.images.objects').'/'.$fullsize);

                $img = Intervention::make($file->getRealPath());
                $img->resize(200, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.objects').'/'.$preview);

                $image = new Image();
                $image->fill([
                    'preview' => $preview,
                    'fullsize' => $fullsize
                ]);
                $image->save();

                return $this->json([
                    'id' => $image->id,
                    'preview' => $preview,
                    'fullsize' => $fullsize
                ]);

            case 'manager':
                $filename = uniqid().'.'.$ext;
                $img = Intervention::make($file->getRealPath());
                $img->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.managers').'/'.$filename);

                return $this->json([
                    'path' => $filename
                ]);
            case 'office':
                $filename = uniqid().'.'.$ext;
                $img = Intervention::make($file->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.offices').'/'.$filename);

                return $this->json([
                    'path' => $filename
                ]);
            case 'complex':
                $filename = uniqid().'.'.$ext;
                $img = Intervention::make($file->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.complexes').'/'.$filename);

                return $this->json([
                    'path' => $filename
                ]);
            case 'news':
                $filename = uniqid().'.'.$ext;
                $img = Intervention::make($file->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.news').'/'.$filename);

                return $this->json([
                    'path' => $filename
                ]);

            case 'editor':
                $filename = uniqid().'.'.$ext;
                $img = Intervention::make($file->getRealPath());
                $img->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(\Config::get('app.images.editor').'/'.$filename);

                return $this->json([
                    'filelink' => '/images/editor/'.$filename
                ]);
        }

    }

}
