<?php
namespace Admin;

use \Input,
    \Place;

class PlacesController extends \BaseController {

    function all() {
        $places = Place::with('cities')->whereNull('parent_id')->get();
        return $this->json([
            'data' => $places
        ]);
    }

    function get() {
        $id = Input::get('id');

        $place = Place::find($id);

        return [
            'place' => $place,
            'path' => $place->getPath()
        ];
    }

    function save() {
        $data = Input::all();

        if (!empty($data['id'])) {
            $place = Place::find($data['id']);
            $place->update($data);
        }
        else {
            $place = new Place();
            $place->fill($data);
            $place->save();
        }

        return $this->json([]);
    }

}