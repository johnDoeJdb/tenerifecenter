<?php

class PageController extends BaseController {

    public function show() {
        $path = explode('/', Route::input('path'));
        $pages = Page::getList();

        $lang = App::getLocale();

        $tree = $this->buildTree($pages, $lang);
        $tree = current($tree);

        /**
         * Поиск запрошенной страницы
         */
        $page = $tree;
        $breadcrumbs = [];
        $segments = [];
        foreach ($path as $segment) {
            if (!isset($page->children[$segment])) {
                App::abort(404);
            }
            $page = $page->children[$segment];
            $segments[] = $segment;
            $breadcrumbs[] = [join('/', $segments), Page::getLocal($page, 'name')];
        }

        /**
         * Меню
         */
        $url_prefix = current($breadcrumbs)[0];
        //dd($url_prefix);
        $menu = $tree->children[$path[0]]->children;

        $page = Page::find($page->id);

        return View::make('site.page')->with([
            'page' => $page,
            'menu' => $menu,
            'url_prefix' => $url_prefix,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    private function buildTree($dataset, $locale) {
        $tree = [];
        $references = [];
        foreach ($dataset as $id => &$node) {
            $references[$node->id] = &$node;
            $node->children = array();
            if (is_null($node->parent_id)) {
                $tree[$node->{"path-$locale"}] = &$node;
            } else {
                $references[$node->parent_id]->children[$node->{"path-$locale"}] = &$node;
            }
        }
        return $tree;
    }

}
