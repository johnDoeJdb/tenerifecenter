<?php

class ApiController extends BaseController {

    public function search() {

        $favorites = Input::get('favorites');

        $for = Input::get('for', 'sale');
        $price_from = Input::get('price_from');
        $price_to = Input::get('price_to');
        $type = Input::get('type');
        $place = Input::get('place');
        $complex = Input::get('complex');
        $tag = Input::get('tag', []);
        if ($type) {
            $tag[Config::get('object.tags.type')] = $type;
        }

        $l = App::getLocale();

        $tagQuery = function($query) use (&$l) {
            $query
                ->orderBy('order', 'asc')
                ->where('type', '=', 'feature')
                ->get(['tags.id', "name-$l as name", 'icon_class', 'rent', 'sale', 'search']);
        };

        $imageQuery = function($query) {
            $query
                ->orderBy('order', 'asc')
                ->get(['images.id', 'fullsize', 'preview']);
        };

        $query = Object::with(['tags' => $tagQuery, 'images' => $imageQuery]);

        if (is_array($favorites)) {
            if ($favorites) {
                $query->whereIn('id', $favorites);
            }
            else {
                return $this->json([
                    'data' => []
                ]);
            }
        }
        else {

            $query->where('for', '=', $for);

            $query->whereNotNull('public_id');

            if ($tag) {
                $query->whereHas('tags', function($query) use (&$tag) {
                    $query->whereIn('tag_id', $tag);
                });
            }
            if ($price_from && is_numeric($price_from)) {
                $query->where('price', '>=', $price_from);
            }
            if ($price_to && is_numeric($price_to)) {
                $query->where('price', '<=', $price_to);
            }
            if ($place) {
                $query->where('place_id', '=', $place);
            }
            if ($complex) {
                $query->where('complex_id', '=', $complex);
            }
        }

        $objects = $query->get([
            'id',
            "name-$l as name",
            "teaser-$l as teaser",
            'price',
            'lat',
            'lng'
        ]);

        return $this->json([
            'data' => $objects
        ]);
    }

    public function object($id) {

        $l = App::getLocale();

        $tagQuery = function($query) use (&$l) {
            $query
                ->orderBy('order', 'asc')
                ->where('type', '=', 'feature')
                ->get(['tags.id', 'tags.group', 'tags.parent_id', "name-$l as name", "description-$l as description", 'icon_class', 'rent', 'sale', 'search']);
        };

        $tagParentQuery = function($query) use (&$l) {
            $query->get(['tags.id', "name-$l as name", "description-$l as description", 'icon_class', 'rent', 'sale', 'search']);
        };

        $imageQuery = function($query) {
            $query
                ->orderBy('order', 'asc')
                ->get(['images.id', 'fullsize', 'preview']);
        };

        $object = Object::with([
            'tags' => $tagQuery,
            'tags.parent' => $tagParentQuery,
            'images' => $imageQuery,
            'complex',
            'complex.place',
            'place'
        ])->find($id);

        return View::make('site.object')->with([
            'object' => $object
        ]);
    }

    public function offers() {
        $type = Request::get('type');

        $objects = Tag::getObjects();

        Response::json(array('name' => 'Steve', 'state' => 'CA'));

        return View::make('site.object')->with([
            'object' => $object
        ]);
    }

}
