<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function json($data, $status = 200, $headers = []) {
        return Response::json($data, $status, $headers, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
    }

}
