<?php

/**
 * Image
 *
 */
class Image extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'images';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['pivot'];

    public function objects() {
        return $this->belongsToMany('Object', 'objects_images', 'image_id', 'object_id');
    }

}
