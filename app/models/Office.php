<?php

class Office extends Model {

    protected $table = 'offices';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['pivot'];

    public function place() {
        return $this->belongsTo('Place');
    }

    public function managers() {
        return $this->belongsToMany('User');
    }
}
