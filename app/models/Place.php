<?php

/**
 * Place
 *
 */
class Place extends Model {

    protected $table = 'places';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'pivot'];

    public function cities() {
        return $this->hasMany('Place', 'parent_id');
    }

    static function getList() {
        $fields = array_merge(['id', 'parent_id'], self::generateFields('name'));
        $data = DB::table('cities')->select($fields)->get();
        return $data;
    }

    static function getForSelect($lang) {
        $result = [];
        $nameField = 'name-'.$lang;
        $data = DB::table('places as p')->get();
        foreach ($data as $item) {
            $result[] = [
                'id' => $item->id,
                'name' => $item->{$nameField},
                'lat' => $item->lat,
                'lng' => $item->lng,
            ];
        }
        return $result;
    }

    static function getPlacesNames($lang) {
        $result = [];
        $result[0] = '-';
        $places = DB::table('places')->select('id', "name-$lang as name")->get();
        foreach ($places as $place) {
            $result[$place->id] = $place->name;
        }
        return $result;
    }

    static function getPopularPlacesNames($lang) {
        $objectsQuery = function($query) {
            $query->orderBy('order', 'asc')
                ->get(['images.id', 'fullsize', 'preview']);
        };
        $places = DB::table('places as p')
            ->select('p.id', "p.name-$lang as name", DB::raw('COUNT(p.id) as count'))
            ->join('objects', 'objects.place_id', '=', 'p.id')
            ->groupBy('p.id')
            ->orderBy('count', 'DESC')
            ->take(10)
            ->get();
        $result = [];
        foreach ($places as $place) {
            $result[$place->id] = $place->name;
        }
        return $result;
    }

    function getPath(array $fields = []) {
        return parent::_getPath(array_merge(['id'], self::generateFields('name')));
    }
}
