<?php

/**
 * Tag
 *
 */
class Tag extends Model {

    protected $table = 'tags';

    protected $guarded = ['id', 'group', 'parent_id', 'type', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'pivot'];

    public function parent() {
        return $this->belongsTo('Tag', 'parent_id');
    }

    public function options() {
        return $this->hasMany('Tag', 'parent_id');
    }

    public function objects() {
        return $this->belongsToMany('Object', 'objects_tags', 'tag_id', 'object_id');
    }

    function getPath() {
        return $this->_getPath(array_merge(['id'], self::generateFields('name')));
    }

    static function getList() {
        $fields = array_merge(['id', 'type'], self::generateFields('name', 'description'));
        $data = self::select($fields)->get();

        return $data;
    }

    static function getPropertyTypesNames($language) {
        $propertyTypes = Tag::where('parent_id', '=', Config::get('object.tags.type'))->get(['id', "name-$language as name"]);
        $result = [];
        $result[0] = '-';
        foreach ($propertyTypes as $item) {
            $result[$item->id] = $item->name;
        }

        return $result;
    }

    static function getPopularPropertyTypesNames($language) {
        $popularPropertyTypes =
            DB::table('tags as t')
            ->select('ot.tag_id as id', "t.name-{$language} as name", DB::raw('COUNT(ot.tag_id) as count'))
            ->where('t.parent_id', '=', Config::get('object.tags.type'))
            ->join('objects_tags as ot', 't.id', '=', 'ot.tag_id')
            ->groupBy('ot.tag_id')
            ->orderBy('count', 'DESC')
            ->take(8)
            ->get();
        $result = [];
        foreach ($popularPropertyTypes as $item) {
            $result[$item->id] = $item->name;
        }

        return $result;
    }

    static function getObjects($page = 1) {
        $result = [];
        $tag = Tag::find(1);
        DB::getPaginator()->setCurrentPage($page);
        if ($tag) {
            $imageQuery = function($query) {
                $query->orderBy('order', 'asc')
                    ->get(['images.id', 'fullsize', 'preview']);
            };
            $result = $tag->objects()
                ->with(['images' => $imageQuery])
                ->has('images')
                ->paginate(8)
            ;
        }

        return $result;
    }

    static function getObjectsByType($type = null) {
        $result = [];
        $items = [];
        $tag = Tag::find(1);
        if ($tag) {
            $imageQuery = function($query) {
                $query->orderBy('order', 'asc')->get(['images.id', 'fullsize', 'preview']);
            };
            $result = $tag
                ->objects()
                ->with(['images' => $imageQuery])
                ->has('images')
                ->orderBy('id', 'desc')
            ;
            if ($type) {
                $result = $result->where('for', $type);
            }
        }

        $nameField = 'name-'.App::getLocale();
        foreach ($result->limit(100)->get() as $item) {
            $newItem = $item->original;
            $newItem['name'] = $newItem[$nameField];
            $newItem['image'] = \Tenerife\Blade\BladeHelper::imageResize('images/objects/'.$item->images[0]->preview, 248, 186);
            $items[] = $newItem;
        }

        return $items;
    }

}
