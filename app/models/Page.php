<?php

/**
 * Page
 *
 */
class Page extends Model {

	protected $table = 'pages';

    protected $guarded = ['id', 'parent_id', 'created_at', 'updated_at', 'deleted_at'];

    static function getList() {
        $fields = array_merge(['id', 'parent_id'], self::generateFields('path', 'name'));
        $data = DB::table('pages')->select($fields)->get();

        return $data;
    }

    function getPath() {
        return $this->_getPath(array_merge(['id'], self::generateFields('name')));
    }
}
