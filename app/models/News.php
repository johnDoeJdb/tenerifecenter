<?php

/**
 * News
 *
 */
class News extends Model {

    protected $table = 'news';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function images() {
        return $this->belongsToMany('Image', 'objects_images', 'object_id', 'image_id')->orderBy('order', 'asc');
    }

    static function getList(array &$params, &$total) {
        $request = self::select('*');

        $params['page'] = isset($params['page']) && is_numeric($params['page']) ? $params['page'] : 1;
        $params['per_page'] = isset($params['per_page']) && is_numeric($params['per_page']) ? $params['per_page'] : 20;

        $total = $request->count();

        $request->orderBy('created_at', 'desc');
        $request->limit($params['per_page']);
        $request->offset(($params['page'] - 1) * $params['per_page']);

        $data = $request->get();

        return $data;
    }

}
