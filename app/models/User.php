<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * User
 *
 */
class User extends Model implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

    protected $guarded = ['id', 'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'];

	protected $table = 'users';

	protected $hidden = ['password', 'remember_token'];

	public function offices() {
		return $this->belongsToMany('Office', 'offices_users', 'user_id', 'office_id');
	}
}
