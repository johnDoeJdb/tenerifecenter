<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

abstract class Model extends Eloquent {

    use SoftDeletingTrait;

    protected function getDateFormat() {
        return 'U';
    }

    protected function _getPath(array $fields = []) {
        $tries = 10;
        $pages = [];
        $parent_id = $this->parent_id;
        while ($parent_id && $tries-- > 0) {
            $page = self::find($parent_id, $fields);
            $parent_id = $page->parent_id;
            $pages[] = $page->toArray();
        }
        $pages = array_reverse($pages);
        return $pages;
    }

    protected static function generateFields() {
        $fields = [];
        foreach (Config::get('app.locales') as $lang) {
            foreach (func_get_args() as $field) {
                $fields[] = "$field-$lang";
            }
        }
        return $fields;
    }

    static function getLocal($object, $field, $locale = null) {
        $locales = Config::get('app.locales');
        $locale = $locale ?: App::getLocale();
        if (!empty($object->{"$field-$locale"})) {
            return $object->{"$field-$locale"};
        }
        foreach ($locales as $l) {
            if (!empty($object->{"$field-$l"})) {
                return $object->{"$field-$l"};
            }
        }
        return null;
    }
}