<?php

/**
 * Object
 *
 */
class Object extends Model {

    protected $table = 'objects';

    protected $guarded = ['id', 'public_id', 'manager_id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['pivot'];

    public function tags() {
        return $this->belongsToMany('Tag', 'objects_tags', 'object_id', 'tag_id');
    }

    public function complex() {
        return $this->belongsTo('Complex', 'complex_id');
    }

    public function place() {
        return $this->belongsTo('Place', 'place_id');
    }

    public function manager() {
        return $this->belongsTo('User', 'manager_id');
    }

    public function images() {
        return $this->belongsToMany('Image', 'objects_images', 'object_id', 'image_id')->orderBy('order', 'asc');
    }

    public function publish() {
        if ($this->public_id) {
            return true;
        }
        if (empty($this->for)) {
            throw new Exception('Unknown for field');
        }

        $published = self::where('for', '=', $this->for)
            ->whereNotNull('public_id')
            ->orderBy('public_id')
            ->get(['public_id']);

        // найдем первый свободный номер
        $shouldBeNext = $this->for == 'rent' ? 1 : 501;
        foreach ($published ?: [] as $o) {
            if ($o->public_id != $shouldBeNext) {
                break;
            }
            $shouldBeNext++;
        }

        $foundId = $shouldBeNext;

        $this->public_id = $foundId;
        $this->save();
    }

    public function unpublish() {
        $this->public_id = null;
        $this->save();
    }

    static public function getPropertyType($objectId) {
        $tagQuery = function($query) {
            $query
                ->orderBy('order', 'asc')
                ->where('parent_id', '=', Config::get('object.tags.type'))
                ->get(['id', "name-ru as name"]);
        };
        $tags = self::find($objectId)
            ->with(['tags' => $tagQuery])
            ->first()
        ;

        var_dump($tags);die;
    }

    static function getList(array &$params, &$total) {
        $fields = array_merge(['id', 'for', 'public_id'], self::generateFields('name'));
        $request = self::select($fields);

        if (isset($params['manager']) && is_numeric($params['manager'])) {
            $request->where('manager_id', '=', $params['manager']);
        }

        if (!empty($params['query'])) {
            $query = $params['query'];
            $fields = self::generateFields('name', 'teaser', 'description');
            $values = array_merge([$query], array_fill(0, count($fields), "%$query%"));
            $request->whereRaw('public_id = ? OR `'.join('` LIKE ? OR `', $fields).'` LIKE ?', $values);
        }

        $params['page'] = isset($params['page']) && is_numeric($params['page']) ? $params['page'] : 1;
        $params['per_page'] = isset($params['per_page']) && is_numeric($params['per_page']) ? $params['per_page'] : 20;

        $total = $request->count();

        $request->orderBy('updated_at', 'desc');
        $request->limit($params['per_page']);
        $request->offset(($params['page'] - 1) * $params['per_page']);

        $data = $request->get();

        return $data;
    }

}
