<?php

class Complex extends Model {

    protected $table = 'complexes';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['pivot'];

    public function place() {
        return $this->belongsTo('Place', 'place_id');
    }

    static function getComplexesNames($lang) {
        $result = [];
        $result[0] = '-';
        $complexes = DB::table('complexes')->select('id', "name-$lang as name")->get();
        foreach ($complexes as $complex) {
            $result[$complex->id] = $complex->name;
        }
        return $result;
    }
}
