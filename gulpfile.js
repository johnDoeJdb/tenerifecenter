var gulp = require('gulp'),
    minifyCss = require("gulp-minify-css"),
    uglify = require("gulp-uglify"),
    concat = require("gulp-concat");

gulp.task('admin-libs-js', function() {
    return gulp.src([
        './bower_components/lodash/lodash.js',
        './bower_components/jquery/dist/jquery.js',
        './bower_components/angular/angular.js',
        './bower_components/angular-route/angular-route.js',
        './bower_components/angular-bootstrap/ui-bootstrap.js',
        './bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        './bower_components/angular-utils-pagination/dirPagination.js',
        './bower_components/angular-mask/dist/ngMask.js',
        './bower_components/moment/moment.js',
        './bower_components/moment/locale/ru.js',
        './bower_components/moment/locale/en.js',
        './bower_components/moment/locale/es.js',
        './bower_components/moment/locale/de.js',
        './app/assets/javascript/redactor/redactor.min.js',
        './app/assets/javascript/redactor/table.js',
        './app/assets/javascript/redactor/imagemanager.js',
        './app/assets/javascript/redactor/ru.js',
        './app/assets/javascript/redactor/angular-redactor.js',
    ])
        .pipe(concat('admin-libs.js'))
        .pipe(gulp.dest('./public/assets/javascript/'))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('./public/assets/javascript/'));
});

gulp.task('admin-js', function() {
    return gulp.src([
        './app/assets/javascript/admin.js'
    ])
        .pipe(concat('admin.js'))
        .pipe(gulp.dest('./public/assets/javascript/'))
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('./public/assets/javascript/'));
});

gulp.task('site-libs-js', function() {
    return gulp.src([
        './bower_components/angular/angular.min.js',
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/lightbox2/dist/js/lightbox-plus-jquery.min.js',
        './bower_components/jcarousel/dist/jquery.jcarousel.min.js',
        './bower_components/tether/dist/js/tether.min.js',
        './bower_components/tether-select/dist/js/select.min.js',
        './app/assets/includes/jcarousel.responsive.js',
        './bower_components/jquery-ui/ui/datepicker.js',
    ])
        .pipe(concat('site-libs.js'), {newLine: ';\r\n'})
        .pipe(gulp.dest('./public/assets/javascript/'))
        .pipe(uglify({'managle': false}))
        .pipe(gulp.dest('./public/assets/javascript/'));
    ;
});

gulp.task('site-js', function() {
    return gulp.src([
        //'./app/assets/includes/index-map.js',
        //'./app/assets/includes/rent-map.js',
        //'./app/assets/includes/sale-map.js',
        //'./app/assets/includes/scripts.js'
        './app/assets/javascript/site.js'
    ])
        .pipe(concat('site.js'))
        .pipe(gulp.dest('./public/assets/javascript/'))
        .pipe(uglify({'managle': false}))
        .pipe(gulp.dest('./public/assets/javascript/'));
});

gulp.task('site-libs-css', function() {
    return gulp.src([
        './app/assets/includes/normalize.css',
        './app/assets/includes/select/select-theme-default.css',
        './app/assets/includes/select/select-theme-action.css',
        './app/assets/includes/select/select-theme-time.css',
        './app/assets/includes/select/select-theme-cur.css',
        './app/assets/includes/select/select-theme-comis.css',
        './app/assets/includes/jcarousel.responsive.css'
    ])
        .pipe(concat('site-libs.css'))
        .pipe(gulp.dest('./public/assets/stylesheets/'))
        .pipe(minifyCss())
        .pipe(gulp.dest('./public/assets/stylesheets/'));
    ;
});

gulp.task('site_libs', ['site-libs-js', 'site-libs-css']);
gulp.task('site-all', ['site-js', 'site-libs-js', 'site-libs-css']);
gulp.task('admin_libs', ['admin-libs-js']);
gulp.task('admin', ['admin-js']);
gulp.task('default', ['site-js', 'site-libs-js', 'site-libs-css'], function() {});
gulp.task('watch', function() {gulp.watch('./app/assets/javascript/site.js', ['site-js']);});


